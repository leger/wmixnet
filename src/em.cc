// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

struct wmixnet::job_em_init
{
    wmixnet* p_obj;
    int Q;
    void operator()()
    {

        if(p_obj->v_J(Q-p_obj->get_Qmin())==-INFINITY)
        {
            std::cerr << "\t" << "Q = " << Q << " ... starting" << std::endl;
            itpp::mat m_tau_init=p_obj->construire_init(Q);


            p_obj->em(m_tau_init,Q);

            std::cerr << "\t" << "Q = " << Q << " ... ";
            std::cerr << "done\t\t\t\t\t";
            std::cerr << " J=";
            std::cerr << p_obj->v_J(Q-p_obj->Qmin);
            std::cerr << std::endl;
            p_obj->save_state();
        }
        else
        {
            std::cerr << "\t" << "Q = " << Q << " ... ";
            std::cerr << "skipped";
            std::cerr << std::endl;
        }
    }
};

void wmixnet::em(itpp::mat m_tau,int Q)
{
    verif_model_defined();
    borner(m_tau,tolP,1-tolP);

    itpp::Vec<itpp::mat> vm_theta = f_init_modele(m_tau);

    itpp::vec v_alpha = itpp::sum(m_tau,1)/m_X.rows();
    
   
    v_alpha = v_alpha * (1/itpp::sum(v_alpha));


    double diff;


    // M step
//    std::cerr << "M step" << std::endl;
    itpp::vec v_alpha2 = v_alpha;
    borner(v_alpha2,tolP,1);
    itpp::Vec<itpp::mat> vm_theta2;
    f_M_modele(v_alpha, m_tau, vm_theta, vm_theta2); // a laisser?, a l'air de faire diverger BH... et planter BI...
    v_alpha = v_alpha2;
    vm_theta=vm_theta2; //pour tenir compte de l'etape M
    

    double oldJ = calc_J(v_alpha,m_tau,vm_theta);


    do
    {

        // E step
               
        itpp::mat m_tau2 = e_step(v_alpha2, m_tau, vm_theta);
        borner(m_tau2,tolP,1-tolP);

        // M step
        itpp::vec v_alpha2 = itpp::sum(m_tau2,1)/m_X.rows();
        borner(v_alpha2,tolP,1);
        v_alpha2 = v_alpha2 * (1/itpp::sum(v_alpha2));
        itpp::Vec<itpp::mat> vm_theta2;
        f_M_modele(v_alpha2, m_tau2, vm_theta, vm_theta2);



        double d_alpha = itpp::max(abs(v_alpha-v_alpha2));
        double d_theta = f_norm_theta_modele(vm_theta, vm_theta2);
        diff = (d_alpha>d_theta) ? d_alpha : d_theta;

        double J = calc_J(v_alpha2,m_tau2,vm_theta2); 
        //std::cerr << "J " << J << std::endl;
        if(J<oldJ)
        {
            //std::cerr << oldJ << "\t" << J << std::endl;

            break;
        }

        oldJ=J;

        m_tau = m_tau2;
        v_alpha = v_alpha2;
        vm_theta = vm_theta2;



//        std::cout << diff << std::endl;
    } while( diff > tolEM );

    double PL = f_PL_modele(v_alpha,m_tau,vm_theta);
    double H = calc_H(m_tau);
    double J=H+PL;
    // On garde les resultats si J est meilleur

    if(J>v_J(Q-Qmin))
    {
            boost::mutex::scoped_lock lock(mutex_donnees);
            int n =m_X.rows();
            double ICL;

            if(sym_done)  
                ICL = PL-0.5*(f_nb_parameters_modele(Q)*log(n*(n-1)/2)+(Q-1)*log(n));
            else 
                ICL = PL-0.5*(f_nb_parameters_modele(Q)*log(n*(n-1))+(Q-1)*log(n));


            vm_tau(Q-Qmin) = m_tau;
            vv_alpha(Q-Qmin) = v_alpha;
            vvm_theta(Q-Qmin) = vm_theta;
            v_J(Q-Qmin) = J;
            v_H(Q-Qmin) = H;
            v_PL(Q-Qmin) = PL;
            v_ICL(Q-Qmin) = ICL;
            v_id(Q-Qmin)++;
            if(Q==1)
                m_coords_SC0=spectral_clustering_coords(f_residuelle_modele(m_tau,vm_theta));
    }
}

void wmixnet::em_with_init()
{
    verif_Q_defined();

    std::cerr << "With init of spectral clustering, EM for" << std::endl;

    // Variable structuré utile pour le paralellisme



    int & nthreads = parallel;

    if(parallel>1)
    {
        boost::timer::cpu_timer timer;
        timer.start();
        // On cree un tableau de threads
        boost::thread* tab = new boost::thread[nthreads];
        for(int Q=Qmax;Q>=Qmin;Q--)
        {
            // On les lance dans l'ordre decroissant

            // On boucle jusqu'a trouver un thread de libre
            int num=0;
            do
            {
                num++;
                num=num%nthreads;
            } while(!( tab[num].timed_join(boost::posix_time::milliseconds(10)) ));

            // num est un thread libre ! Au boulot feignasse !
            struct wmixnet::job_em_init le_job;
            le_job.p_obj=this;
            le_job.Q=Q;
            tab[num] = boost::thread(le_job);
        }

        // Maintenant on attend que tous les jobs aient fini
        for(int num=0;num<nthreads;num++)
        {
            tab[num].join();
        }
        // Done
        delete[] tab;


        std::cerr << "CPU time" << "\t\t\t\t\t\t";
        timer.stop();
        double tps=(double)(timer.elapsed().user+ timer.elapsed().system)/1000000000LL;
        wmixnet::writetmp(tps);
        std::cerr << "\n";
                
    }
    else
    {
        for(int Q=Qmin;Q<=Qmax;Q++)
        {
            boost::timer::cpu_timer timer;
            timer.start();

            struct wmixnet::job_em_init le_job;
            le_job.p_obj=this;
            le_job.Q=Q;
            le_job();

            std::cerr << "CPU time" << "\t\t\t\t\t\t";
            timer.stop();
            double tps=(double)(timer.elapsed().user+ timer.elapsed().system)/1000000000LL;
            wmixnet::writetmp(tps);
            std::cerr << "\n";
        }
    }


    em_init = true;
}

