// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef H_WMIXNET_MODELES
#define H_WMIXNET_MODELES 1

#include <itpp/itbase.h>

class model
{
    public:

        // pointeur vers une fonction pour le point fixe de l'E step
        double ( *pseudoEstep ) ( itpp::vec v_alpha, itpp, itpp::mat m_tau, itpp::Vec<itpp::mat> vm_theta);


};

#endif
