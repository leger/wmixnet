// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef H_ITPP_SERIALIZE
#define H_ITPP_SERIALIZE

namespace boost {
namespace serialization {
template<class Archive, class T>
void serialize(Archive & ar, itpp::Mat<T> & m, const unsigned int version)
{
    if (Archive::is_loading::value)
    {
        int nr, nc;
        ar & nr;
        ar & nc;
        m.set_size(nr,nc,false);
    }
    else
    {
        int nr = m.rows();
        int nc = m.cols();
        ar & nr;
        ar & nc;
    }
    for(int i=0;i<m._datasize();i++)
        ar & m._elem(i);
}

template<class Archive, class T>
void serialize(Archive & ar, itpp::Vec<T> & m, const unsigned int version)
{
    if (Archive::is_loading::value)
    {
        int n;
        ar & n;
        m.set_size(n,false);
    }
    else
    {
        int n = m.size();
        ar & n;
    }
    for(int i=0;i<m.size();i++)
        ar & m._elem(i);
}
} // namespace serialization
} // namespace boost

#endif
