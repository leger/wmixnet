// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

inline itpp::mat read_beta_a(const itpp::Vec<itpp::mat> & vm_theta,int q, int l)
{
    int p=vm_theta.size()-2;
    itpp::mat m_beta(1,p+1);
    for(int i=0;i<p+1;i++)
        m_beta(i)=vm_theta(i)(q,l);
    return(m_beta);
}

inline void write_beta_a(itpp::Vec<itpp::mat> & vm_theta,int q, int l, const itpp::mat & m_beta)
{
    int p=vm_theta.size()-2;
    for(int i=0;i<p+1;i++)
        vm_theta(i)(q,l)=m_beta(i);
}

inline itpp::mat augmenter_mat_c(const itpp::mat m_orig, const double val)
{
    itpp::mat m_dest(m_orig.rows()+1,1);
    m_dest(0)=val;
    for(int k=0;k<m_orig.rows();k++)
        m_dest(k+1)=m_orig(k);

    return m_dest;
}

inline itpp::mat augmenter_mat_l(const itpp::mat m_orig, const double val)
{
    return augmenter_mat_c(m_orig.T(),val).T();
}

itpp::Vec<itpp::mat> wmixnet::f_init_GRMI(const itpp::mat &m_tau)
{
    int p=mm_Y(1).rows();
    itpp::Vec<itpp::mat> vm_theta(p+2);

    itpp::mat &m_mu = vm_theta(0);
    itpp::mat m_A = m_tau.T() *m_Xzd *m_tau;
    itpp::mat m_B = m_tau.T() *m_nodiag *m_tau;
    m_mu = itpp::elem_div(m_A, m_B);

    for(int i=1;i<p+1;i++)
    {
        itpp::mat &m_mat = vm_theta(i);
        m_mat.set_size(m_tau.cols(),m_tau.cols());
        m_mat.zeros();
    }

    vm_theta(p+1).set_size(1,1);
    double &sigma2 = vm_theta(p+1)(0);

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_mu.rows();i++)
        for(int j=0;j<m_mu.cols();j++)
            if(isnan(m_mu(i,j)))
                m_mu(i,j)=0;

    int n = m_X.rows();
    double s=0;
    s+= itpp::sumsum(m_tau.T()*m_Xzd2*m_tau);
    s+= itpp::elem_mult_sum(itpp::elem_mult(m_mu,m_mu),m_B);
    s+= -2*itpp::elem_mult_sum(m_mu, m_A);
    s/= n*(n-1);

    sigma2=s;
    return vm_theta;
}

inline double lfphi(double x,double m, double s2)
{
    return(-0.5*log(2*M_PI*s2)-0.5*(x-m)*(x-m)/s2);
}
    

void wmixnet::f_E_GRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();
    int p=mm_Y(1).rows();

    // tout d'abort nomons les choses par leurs noms.
    const double &sigma2 = vm_theta(p+1)(0);
    

    m_tau2 = repmat(log(v_alpha),n,1,true);

//    std::cerr << m_X(0,1) << std::endl;
//    std::cerr << mm_Y(0,1) << std::endl;
//    std::cerr << m_beta << std::endl;
    for(int i=0;i<m_tau2.rows();i++)
    {
        for(int j=0;j<m_tau2.rows();j++)
        {
            if(i!=j)
            {
                itpp::mat m_Yij_a = augmenter_mat_c(mm_Y(i,j),1);
                itpp::mat m_Yji_a = augmenter_mat_c(mm_Y(j,i),1);
                for(int q=0;q<m_tau2.cols();q++)
                {
                    for(int l=0;l<m_tau2.cols();l++)
                    {
                        itpp::mat m_betaql_a = read_beta_a(vm_theta,q,l);
                        itpp::mat m_betalq_a = read_beta_a(vm_theta,l,q);
                        double f=0;
                        f+=lfphi(m_X(i,j),(m_betaql_a*m_Yij_a)(0),sigma2);
                        if(!sym_done)
                            f+=lfphi(m_X(j,i),(m_betalq_a*m_Yji_a)(0),sigma2);
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    }

    
    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

void wmixnet::f_M_GRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{
    int p=mm_Y(1).rows();
    vm_theta2 = vm_theta;
    double &sigma2 = vm_theta2(p+1)(0);

//    std::cout << vm_theta2 << std::endl << std::endl;
    
    // On a un maximum explicite pour chaque q,l

    for(int q=0;q<m_tau.cols();q++)
    {
        for(int l=0;l<m_tau.cols();l++)
        {
            // On contruit les matrices m_A et m_b
            itpp::mat m_A(p+1,p+1);
            itpp::mat m_b(p+1,1);
            m_A.zeros();
            m_b.zeros();

            for(int i=0;i<m_X.rows();i++)
            {
                for(int j=0;j<m_X.rows();j++)
                {
                    if(i!=j)
                    {
                        itpp::mat m_Yij_a = augmenter_mat_c(mm_Y(i,j),1);

                        m_A += m_tau(i,q)*m_tau(j,l)*m_Yij_a*m_Yij_a.T();
                        m_b += m_tau(i,q)*m_tau(j,l)*m_X(i,j)*m_Yij_a;
                    }
                }
            }
            itpp::mat m_betaql_a;
            if(!ls_solve_chol(m_A,m_b,m_betaql_a))
            {
                std::cerr << "LS solve error" << std::endl;
            }

            write_beta_a(vm_theta2,q,l,m_betaql_a.T());
        }
    }

    itpp::mat m_residuelle = f_residuelle_GRMI(m_tau,vm_theta2);
    internal_zd(m_residuelle);
    sigma2 = itpp::elem_mult_sum(m_residuelle,m_residuelle)/(m_X.rows()*(m_X.rows()-1));

    

    
}

double wmixnet::f_norm_theta_GRMI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    int p=mm_Y(1).rows();
    double diff=0;
    for(int i=0;i<p+1;i++)
    {
        double d = itpp::max(itpp::max(itpp::abs(vm_theta1(i)-vm_theta2(i))));
        diff = (diff>d) ? diff : d;
    }


    return diff;
}

double wmixnet::f_norm2_classe_GRMI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{

    double diff=0;
    int p=mm_Y(1).rows();

    for(int i=0;i<p+1;i++)
    {
        const itpp::mat & m_mat = vm_theta(i);
        itpp::vec v_diff = m_mat.get_col(q)-m_mat.get_col(l);
        itpp::vec v_diffr = m_mat.get_row(q)-m_mat.get_row(l);

        diff += elem_mult_sum(v_diff,v_diff);
        diff += elem_mult_sum(v_diffr,v_diffr);
    }

    return diff;
}

double wmixnet::f_PL_GRMI(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{

    int p=mm_Y(1).rows();
    const double &sigma2 = vm_theta(p+1)(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    
    double mult = (sym_done) ? .5 : 1;
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int j=0;j<m_tau.rows();j++)
        {
            if(i!=j)
            {
                itpp::mat m_Yij_a = augmenter_mat_c(mm_Y(i,j),1);
                for(int q=0;q<m_tau.cols();q++)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        itpp::mat m_betaql_a = read_beta_a(vm_theta,q,l);
                        
                        double f=lfphi(m_X(i,j),(m_betaql_a*m_Yij_a)(0),sigma2);
                        PL += mult*f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    }


    return PL;
}

int wmixnet::f_nb_parameters_GRMI(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2*(1+mm_Y(0,1).rows())+1);
    else
        return (Q*Q*(1+mm_Y(0,1).rows())+1);
}

itpp::mat wmixnet::f_residuelle_GRMI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    itpp::mat m_prediction(m_tau.rows(),m_tau.rows());
    m_prediction.zeros();

    for(int i=0;i<m_tau.rows();i++)
    {
        for(int j=0;j<m_tau.rows();j++)
        {
            if(i!=j)
            {
                itpp::mat m_Yij_a = augmenter_mat_c(mm_Y(i,j),1);
                for(int q=0;q<m_tau.cols();q++)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        itpp::mat m_betaql_a = read_beta_a(vm_theta,q,l);
                        m_prediction(i,j)+=((m_betaql_a*m_Yij_a)(0))*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    }
    
    itpp::mat m_residuelle = m_X - m_prediction;
    return m_residuelle;
}

