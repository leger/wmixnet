// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

itpp::mat wmixnet::construire_init(int Q)
{
    if(Q==1)
    {
        itpp::mat m_tau(m_X.rows(),1);
        return m_tau;
    }

    if(m_coords_SC0.cols()<1)
    {
        std::cerr << "Residuelle non trouve" << std::endl;
        abort;
    }

    itpp::ivec v_all(m_X.cols());
    for(int i=0;i<m_X.rows();i++)
        v_all(i)=i;

    itpp::Vec<itpp::ivec> vv_grps = spectral_clustering_kmeans(m_coords_SC0,v_all,Q,Q);

    // Maintenant on construit tau
    itpp::mat m_tau(m_X.rows(),Q);
    m_tau.zeros();
    
    for(int q=0;q<Q;q++)
        for(int k=0;k<vv_grps(q).length();k++)
            m_tau(vv_grps(q)(k),q)=1;

    borner(m_tau,tolP,1-tolP);

    return(m_tau);
}
