// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::automatic()
{
    if(smoothing_mode=="none")
    {
        std::cerr << "You running automatic mode without smoothing, you must carrefully analalyze results" << std::endl;
    }

    if(Qmax<4 || Qmin<=0)
        set_QminQmax(1,4);

    while(true)
    {
        std::cerr << "Automatic procedure for " << Qmin << " <= Q <= " << Qmax << std::endl;
        em_with_init();
        smoothing();

        int Qopt = itpp::max_index(v_ICL)+Qmin;
        std::cerr << "Current maximum is for Q = " << Qopt << std::endl;

        int Qmaxopti = (exploration*Qopt+.5<m_X.rows()) ? (exploration*Qopt+.5) : m_X.rows();

        if(Qmax>=Qmaxopti)
            break;

        int oldQmax=Qmax;
        set_QminQmax(1,Qmaxopti);

        save_state();
    }
}






    
