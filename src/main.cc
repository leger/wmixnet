// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "main.h"

int main(int argc, char **argv)
{
    itpp::RNG_randomize();
    int c;
    int symetric = false;
    std::string input="";
    int Qmin=0;
    int Qmax=0;
    std::string modele="";
    std::string smoothing_mode="";
    std::string output="";
    std::string format="";
    std::string soptname;
    std::string state_file="";
    double tolF=0;
    double tolP=0;
    double penality=0;
    double tolEM=0;
    double exploration=0;
    bool automatic=false;
    bool ortho=false;
    int number_of_nodes = 0;
    int parallel=boost::thread::hardware_concurrency();

    while (1)
    {
        static struct option long_options[] =
        {
            {"symetric", no_argument,       0, 's'},
            {"symmetric", no_argument,       0, 's'},
            {"Qmax",  required_argument, 0, 'Q'},
            {"input",    required_argument, 0, 'i'},
            {"model",    required_argument, 0, 'm'},
            {"smoothing", required_argument, 0, 'S'},
            {"tolP",required_argument,0,0},
            {"tolF",required_argument,0,0},
            {"tolEM",required_argument,0,0},
            {"penality",required_argument,0,0},
            {"penalty",required_argument,0,0},
            {"output",required_argument,0,'o'},
            {"output-format",required_argument,0,'F'},
            {"state-file",required_argument,0,'f'},
            {"parallel",no_argument,0,'p'},
            {"no-parallel",no_argument,0,'P'},
            {"threads",required_argument,0,'n'},
            {"Qautomatic",no_argument,0,'a'},
            {"Qauto",no_argument,0,'a'},
            {"exploration",required_argument,0,'e'},
            {"number-of-nodes",required_argument,0,'N'},
            {"ortho",no_argument,0,0},
            {"version",no_argument,0,'V'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "sQ:i:m:S:o:f:F:pPn:ae:N:V",
                long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;

                soptname=long_options[option_index].name;
                if(soptname=="tolP")
                {
                    tolP = atof(optarg);
                }
                if(soptname=="tolF")
                {
                    tolF = atof(optarg);
                }
                if(soptname=="tolEM")
                {
                    tolEM = atof(optarg);
                }
                if(soptname=="penality")
                {
                    penality = atof(optarg);
                }
                if(soptname=="penalty")
                {
                    penality = atof(optarg);
                }
                if(soptname=="ortho")
                {
                    ortho = true;
                }
                break;


            case 's':
                symetric = true;
                break;

            case 'Q':
                Qmax = atoi(optarg);
                break;

            case 'i':
                input = optarg;
                break;

            case 'm':
                modele = optarg;
                break;

            case 'S':
                smoothing_mode = optarg;
                break;

            case 'o':
                output = optarg;
                break;

            case 'F':
                format = optarg;
                break;
            
            case 'f':
                state_file = optarg;
                break;

            case 'p':
                parallel = boost::thread::hardware_concurrency();
                break;
            
            case 'P':
                parallel = 1;
                break;
            
            case 'n':
                parallel = atoi(optarg);
                if(parallel<=0)
                    parallel=1;
                break;

            case 'e':
                exploration = atof(optarg);
                break;

            case 'a':
                automatic = true;
                break;

            case 'N':
                number_of_nodes = atoi(optarg);
                break;

            case 'V':
                std::cerr << PACKAGE_STRING << std::endl;
                return(0);
                break;

            case '?':
                /* getopt_long already printed an error message. */
                abort();
                break;

            default:
                abort ();
        }
    }

    Qmin = 1;

    wmixnet wmix;

    // si ni state_file, ni input est defini il y a un pb
    if(input=="" && state_file=="")
    {
        std::cerr << "input file or state file must be defined" << std::endl;
        abort();
    }

    // si le state_file n'est pas defini on le positionne a input+".state"
    if(state_file == "")
    {
        state_file = input+".state";
    }

    // First we load wmix from eventually previous
    wmix.set_state_file(state_file);
    wmix.load_state();

    if(tolF>0)
        wmix.set_tolF(tolF);
    if(tolP>0)
        wmix.set_tolP(tolP);
    if(tolEM>0)
        wmix.set_tolEM(tolEM);
    if(penality>0)
        wmix.set_penality(penality);

    if(exploration>1)
        wmix.set_exploration(exploration);

    if(smoothing_mode!="")
        wmix.set_smoothing_mode(smoothing_mode);

    if(modele!="")
    {
        if(!wmix.model_defined)
        {
            wmix.set_modele(modele);
        }
        else
        {
            std::cerr << "You are not allowed to change model without deleting state file, model flag is ignored" << std::endl;
        }
    }


    if(output!="")
        wmix.set_output(output);

    if(format!="")
        wmix.set_format(format);

    wmix.parallel=parallel;


    // We load the input
    wmix.load_net( input , number_of_nodes );
    if(symetric)
        wmix.symetriser();

    if(ortho)
        wmix.ortho_covariates();

    wmix.precalcul();


    // En tout premier lieu on fait tjs avec 1 groupe,
    wmix.do_first();

    if(!automatic)
    {
        wmix.set_QminQmax(Qmin,Qmax);
        wmix.em_with_init();
        std::cout << wmix.v_J << std::endl;
        wmix.smoothing();
        std::cout << wmix.v_J << std::endl;
    }
    else
    {
        if(Qmax>4 && Qmin>0)
            wmix.set_QminQmax(Qmin,Qmax);

        wmix.automatic();
    }

    wmix.save();


  
    exit (0);
}

