// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

wmixnet::wmixnet()
{
    Qmin = -1;
    Qmax = -1;
    model_defined=false;
    tolEM=1e-5;
    tolF=1e-1;
    tolP=1e-4;
    penality=0;
    em_init=false;
    smoothing_mode="none";
    format="text";
    output="";
    sym_done=false;
    ortho_done=false;
    exploration=1.5;
    parallel=boost::thread::hardware_concurrency();
}

void wmixnet::load_net( std::string input, int number_of_nodes)
{

    std::cerr << "Loading input file ... ";
    
    if(m_X.rows()==0 || m_X.cols()==0)
    {
        std::ifstream ifs_input(input.c_str(), std::ifstream::in);
        itpp::Mat<itpp::vec> mv_Y;


        if(! ifs_input.is_open())
        {
            std::cerr << "Unable to open input file" << std::endl;
            abort();
        }

        clock_t c_start = clock();
        
        if(number_of_nodes>0)
        {
            m_X.set_size(number_of_nodes,number_of_nodes);
            m_X.zeros();
            mv_Y.set_size(number_of_nodes,number_of_nodes);
        }

        std::string str_line;
        
        while(getline(ifs_input,str_line))
        {
            if(str_line[str_line.length()-1] == '\r')
                str_line = str_line.substr(0,str_line.length()-1);

            itpp::vec v_line = str_line;

            if(v_line.size()<3)
            {
                std::cerr << "Each line must have at least 3 columns" << std::endl;
                abort();
            }

            int i = v_line(0)-1;
            int j = v_line(1)-1;

            if(i<0 || j<0)
            {
                std::cerr << "Node indexes must be >0" << std::endl;
                abort();
            }


            int new_size_r = (i+1>m_X.rows()) ? i+1 : m_X.rows();
            int new_size_c = (j+1>m_X.cols()) ? j+1 : m_X.cols();
            int new_size = (new_size_r>new_size_c) ? new_size_r : new_size_c;

            if(m_X.rows()<new_size)
            {
                int old_size=m_X.rows();
                m_X.set_size(new_size,new_size,true);
                mv_Y.set_size(new_size,new_size,true);
                // il faut mettre a ZERO les creations
                for(int ir=old_size;ir<new_size;ir++)
                {
                    for(int jr=0;jr<old_size;jr++)
                    {
                        m_X(ir,jr)=0;
                        m_X(jr,ir)=0;
                    }
                    for(int jr=old_size;jr<new_size;jr++)
                    {
                        m_X(ir,jr)=0;
                    }
                }
            }

            m_X(i,j) = v_line(2);
            if(v_line.size()>3)
                mv_Y(i,j) = itpp::vec(v_line(3,v_line.size()-1));
        }

        int n = (m_X.rows()>m_X.cols()) ? m_X.rows() : m_X.cols();
        m_X.set_size(n,n,true);
        mv_Y.set_size(n,n,true);

        mm_Y.set_size(n,n,false);
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                mm_Y(i,j)=itpp::mat(mv_Y(i,j));





        clock_t c_end = clock();
        double tps = ((double)(c_end-c_start))/CLOCKS_PER_SEC;
        std::cerr << "done\t\t\t\t";
        writetmp(tps);
        std::cerr << std::endl;
    }
    else
    {
        std::cerr << "skipped" << std::endl;
    }
}

void wmixnet::precalcul()
{
    std::cerr << "Precalcul ... ";
    clock_t c_start = clock();
    m_Xzd = internal_zd_c(m_X);
    m_X1zd = internal_zd_c(1-m_X);
    m_XzdT = m_Xzd.T();
    m_X1zdT = m_X1zd.T();

    m_nodiag.set_size(m_X.rows(),m_X.rows());
    m_nodiag.ones();
    internal_zd(m_nodiag);

    m_Xzd2 = itpp::elem_mult(m_Xzd,m_Xzd);


    if(mm_Y(0,1).cols() > 0)
    {
        m_U.set_size(m_X.rows()*(m_X.rows()-1),1);
        m_V.set_size(m_X.rows()*(m_X.rows()-1),mm_Y(0,1).rows());

        {
            int k=0;
            for(int i=0;i<m_X.rows();i++)
            {
                for(int j=0;j<m_X.rows();j++)
                {
                    if(i!=j)
                    {
                        m_U(k,0) = m_X(i,j);
                        for(int p=0;p<mm_Y(0,1).rows();p++)
                        {
                            m_V(k,p)=mm_Y(i,j)(p,0);
                        }
                        k++;
                    }
                }
            }
        }

        itpp::mat m_Prev = m_V.T();
//        std::cerr << m_Prev.rows() << " " << m_Prev.cols() << " ";
//        std::cerr << m_V.rows() << " " << m_V.cols() << " ";
        itpp::mat m_Pro = itpp::operator*(m_Prev,m_V);
        itpp::mat m_Pro2 = itpp::inv(m_Pro);
        itpp::mat m_Pro3 = m_Pro2 * m_V.T();
        m_T = itpp::inv(m_V.T()*m_V) * m_V.T();
    }

    f_precalcul_modele();

    clock_t c_end = clock();
    double tps = ((double)(c_end-c_start))/CLOCKS_PER_SEC;
    std::cerr << "done\t\t\t\t\t";
    writetmp(tps);
    std::cerr << std::endl;

}


int wmixnet::get_Qmin()
{
    return Qmin;
}

int wmixnet::get_Qmax()
{
    return Qmax;
}

itpp::mat wmixnet::get_m_X()
{
    return m_X;
}

itpp::Mat<itpp::mat> wmixnet::get_mm_Y()
{
    return mm_Y;
}

void wmixnet::set_QminQmax(int Qmin_i, int Qmax_i)
{
    if(Qmin_i<=0)
    {
        if(Qmax_i<=0)
        {
            std::cerr << "Qmin or Qmax must be defined" << std::endl;
            abort();
        }
        else
        {
            Qmin_i=Qmax_i;
        }
    }
    else
    {
        if(Qmax_i<=0)
        {
            Qmax_i=Qmin_i;
        }
        else
        {
            if(Qmax_i<Qmin_i)
            {
                std::cerr << "Qmin must be < Qmax" << std::endl;
                abort();
            }
        }
    }

    // Ici Qmin_i et Qmax_i sont definis, peut etre a la meme valeur !
    
    int Qmin_old = Qmin;
    int Qmax_old = Qmax;

    Qmax=Qmax_i;
    Qmin=Qmin_i;

    itpp::Vec<itpp::vec> vv_alpha_old = vv_alpha;
    itpp::Vec<itpp::mat> vm_tau_old = vm_tau;
    itpp::Vec< itpp::Vec<itpp::mat> > vvm_theta_old = vvm_theta;
    itpp::vec v_J_old = v_J;
    itpp::vec v_PL_old = v_PL;
    itpp::vec v_H_old = v_H;
    itpp::vec v_ICL_old = v_ICL;
    itpp::ivec v_id_old = v_id;
    itpp::ivec v_last_ascend_old = v_last_ascend;
    itpp::ivec v_last_descend_old = v_last_descend;

    vv_alpha.set_size(Qmax-Qmin+1);
    vm_tau.set_size(Qmax-Qmin+1);
    vvm_theta.set_size(Qmax-Qmin+1);
    v_J.set_size(Qmax-Qmin+1);
    v_PL.set_size(Qmax-Qmin+1);
    v_H.set_size(Qmax-Qmin+1);
    v_ICL.set_size(Qmax-Qmin+1);
    v_id.set_size(Qmax-Qmin+1);
    v_last_ascend.set_size(Qmax-Qmin+1);
    v_last_descend.set_size(Qmax-Qmin+1);

    for(int Q=Qmax;Q>=Qmin;Q--)
    {
        // A t'on un resultat sur ce Q ?
        if(Q>=Qmin_old && Q<=Qmax_old)
        {
            // Oui
            int old_indice = Q-Qmin_old;
            int new_indice = Q-Qmin;
            vv_alpha(new_indice) = vv_alpha_old(old_indice);
            vm_tau(new_indice) = vm_tau_old(old_indice);
            vvm_theta(new_indice) = vvm_theta_old(old_indice);
            v_J(new_indice) = v_J_old(old_indice);
            v_PL(new_indice) = v_PL_old(old_indice);
            v_H(new_indice) = v_H_old(old_indice);
            v_ICL(new_indice) = v_ICL_old(old_indice);
            v_id(new_indice) = v_id_old(old_indice);
            v_last_descend(new_indice) = v_last_descend_old(old_indice);
            v_last_ascend(new_indice) = v_last_ascend_old(old_indice);
        }
        else
        {
            v_J(Q-Qmin)=-INFINITY;
            v_ICL(Q-Qmin)=-INFINITY;
            v_id(Q-Qmin)=0;
            v_last_ascend(Q-Qmin)=0;
            v_last_descend(Q-Qmin)=0;
        }
    }
}

void wmixnet::set_smoothing_mode(std::string smooth)
{
    if(smooth=="minimal")
        smooth="problems";

    if(smooth=="none" || smooth=="problems" || smooth=="exhaustive")
        smoothing_mode=smooth;
    else
        std::cerr << "Unknown smoothing mode" << std::endl;
}

void wmixnet::set_format(std::string in)
{

    if(in=="matlab")
        in="octave";

    if(in=="text" || in=="octave" || in=="R")
        format=in;
    else
    {
        std::cerr << "Unknown format" << std::endl;
        abort();
    }
}

void wmixnet::set_output(std::string in)
{
    output=in;
}

void wmixnet::set_exploration(double in)
{
    exploration=in;
}

void wmixnet::set_state_file(std::string in)
{
    state_file=in;
}

void wmixnet::set_tolP(double in)
{
    if(in>0)
        tolP=in;
    else
    {
        std::cerr << "tolP must be >0" << std::endl;
        abort();
    }
}

void wmixnet::set_tolF(double in)
{
    if(in>0)
        tolF=in;
    else
    {
        std::cerr << "tolF must be >0" << std::endl;
        abort();
    }
}

void wmixnet::set_tolEM(double in)
{
    if(in>0)
        tolEM=in;
    else
    {
        std::cerr << "tolEM must be >0" << std::endl;
        abort();
    }
}

void wmixnet::set_penality(double in)
{
    if(in>0)
        penality=in;
    else
    {
        std::cerr << "penality must be >0" << std::endl;
        abort();
    }
}


void wmixnet::verif_Q_defined()
{
    if(Qmin==-1 || Qmax==-1)
    {
        std::cerr << "Qmin or Qmax undefined" << std::endl;
        abort();
    }
}

void wmixnet::verif_model_defined()
{
    if(!model_defined)
    {
        std::cerr << "model undefined" << std::endl;
        abort();
    }
}


void wmixnet::verif_em_with_init()
{
    if(!em_init)
    {
        std::cerr << "Initial EM not done" << std::endl;
        abort();
    }
}

void wmixnet::set_modele(const std::string & s_modele)
{
    modele = s_modele;
    if(s_modele == "bernoulli")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_bernoulli;
        p_f_E_modele = &wmixnet::f_E_bernoulli;
        p_f_M_modele = &wmixnet::f_M_bernoulli;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_bernoulli;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_bernoulli;
        p_f_PL_modele = &wmixnet::f_PL_bernoulli;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_bernoulli;
        p_f_residuelle_modele = &wmixnet::f_residuelle_bernoulli;
    }
    
    if(s_modele == "BI")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_BI;
        p_f_E_modele = &wmixnet::f_E_BI;
        p_f_M_modele = &wmixnet::f_M_BI;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_BI;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_BI;
        p_f_PL_modele = &wmixnet::f_PL_BI;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_BI;
        p_f_residuelle_modele = &wmixnet::f_residuelle_BI;
    }
    
    if(s_modele == "BH")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_BH;
        p_f_E_modele = &wmixnet::f_E_BH;
        p_f_M_modele = &wmixnet::f_M_BH;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_BH;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_BH;
        p_f_PL_modele = &wmixnet::f_PL_BH;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_BH;
        p_f_residuelle_modele = &wmixnet::f_residuelle_BH;
    }
  
    if(s_modele == "poisson")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_poisson;
        p_f_E_modele = &wmixnet::f_E_poisson;
        p_f_M_modele = &wmixnet::f_M_poisson;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_poisson;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_poisson;
        p_f_PL_modele = &wmixnet::f_PL_poisson;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_poisson;
        p_f_residuelle_modele = &wmixnet::f_residuelle_poisson;
    }
    
    if(s_modele == "gaussian")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_gaussian;
        p_f_E_modele = &wmixnet::f_E_gaussian;
        p_f_M_modele = &wmixnet::f_M_gaussian;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_gaussian;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_gaussian;
        p_f_PL_modele = &wmixnet::f_PL_gaussian;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_gaussian;
        p_f_residuelle_modele = &wmixnet::f_residuelle_gaussian;
    }
    
    if(s_modele == "PRMH")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_PRMH;
        p_f_E_modele = &wmixnet::f_E_PRMH;
        p_f_M_modele = &wmixnet::f_M_PRMH;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_PRMH;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_PRMH;
        p_f_PL_modele = &wmixnet::f_PL_PRMH;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_PRMH;
        p_f_residuelle_modele = &wmixnet::f_residuelle_PRMH;
    }
    
    if(s_modele == "PRMI")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_PRMI;
        p_f_E_modele = &wmixnet::f_E_PRMI;
        p_f_M_modele = &wmixnet::f_M_PRMI;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_PRMI;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_PRMI;
        p_f_PL_modele = &wmixnet::f_PL_PRMI;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_PRMI;
        p_f_residuelle_modele = &wmixnet::f_residuelle_PRMI;
    }
    
    if(s_modele == "GRMH")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_GRMH;
        p_f_E_modele = &wmixnet::f_E_GRMH;
        p_f_M_modele = &wmixnet::f_M_GRMH;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_GRMH;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_GRMH;
        p_f_PL_modele = &wmixnet::f_PL_GRMH;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_GRMH;
        p_f_residuelle_modele = &wmixnet::f_residuelle_GRMH;
    }
    
    if(s_modele == "GRMI")
    {
        model_defined = true;
        p_f_precalcul_modele = &wmixnet::f_precalcul_nothing;
        p_f_init_modele = &wmixnet::f_init_GRMI;
        p_f_E_modele = &wmixnet::f_E_GRMI;
        p_f_M_modele = &wmixnet::f_M_GRMI;
        p_f_norm_theta_modele = &wmixnet::f_norm_theta_GRMI;
        p_f_norm2_classe_modele = &wmixnet::f_norm2_classe_GRMI;
        p_f_PL_modele = &wmixnet::f_PL_GRMI;
        p_f_nb_parameters_modele = &wmixnet::f_nb_parameters_GRMI;
        p_f_residuelle_modele = &wmixnet::f_residuelle_GRMI;
    }
    
    if(!model_defined)
    {
        std::cerr << "Unknown model" << std::endl;
        abort();
    }
}

double wmixnet::calc_H(const itpp::mat & m_tau)
{
    return -itpp::elem_mult_sum(m_tau,log(m_tau));
}

double wmixnet::calc_J(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    return calc_H(m_tau) + f_PL_modele(v_alpha, m_tau, vm_theta);
}
