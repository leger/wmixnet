// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::save_state()
{
    boost::mutex::scoped_lock lock(mutex_donnees);
    std::ofstream ofs(state_file.c_str());
    
    if(ofs.is_open())
    {
        boost::iostreams::filtering_ostream out;
        out.push(boost::iostreams::gzip_compressor());
        out.push(ofs);
        boost::archive::binary_oarchive oa(out);
        oa << (*this);
    }

    // les fichiers sont fermes a l'appel des destructeurs
}

void wmixnet::load_state()
{
    std::ifstream ifs(state_file.c_str());

    if(ifs.is_open())
    {
        std::cerr << "Loading data from previous state ... ";
        boost::iostreams::filtering_istream in;
        in.push(boost::iostreams::gzip_decompressor());
        in.push(ifs);
        boost::archive::binary_iarchive ia(in);
        ia >> (*this);
        std::cerr << "done" << std::endl;
    }

    // les fichiers sont fermes a l'appel des destructeurs
}
