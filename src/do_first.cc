// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::do_first()
{
    if(m_coords_SC0.cols()<1)
    {
        std::cerr << "Estimation for one group ... ";
        clock_t c_start = clock();

        set_QminQmax(1,1);

        itpp::mat m_tau(m_X.rows(),1);
        m_tau.ones();

        em(m_tau,1);

        clock_t c_end = clock();
        double tps = ((double)(c_end-c_start))/CLOCKS_PER_SEC;
        std::cerr << "done\t\t\t";
        writetmp(tps);
        std::cerr << std::endl;
    }
}

