// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// Copyright (C) 2012, Pierre Barbillon
//               <barbillon@agroparistech.fr>, AgroParisTech, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


/*initialisation de theta */
itpp::Vec<itpp::mat> wmixnet::f_init_BI(const itpp::mat &m_tau)
{
    int Q=m_tau.cols();
    //theta contient les pi_ql et  the matrix
    //beta_{11}, beta_{12} ... beta_{1Q} beta_{21} ... beta_{QQ}
    itpp::Vec<itpp::mat> vm_theta(1+Q*Q); 
    itpp::mat &m_pi = vm_theta(0);
    

    itpp::mat m_prob = itpp::elem_div(m_tau.T() *m_X *m_tau, m_tau.T() * m_nodiag *m_tau);
    m_pi=log(m_prob)-log(1-m_prob);
    //initialisation bete
    //m_pi.set_size(Q,Q);
    //m_pi.zeros();  

    for(int i=0;i<Q*Q;i++)
    {
        itpp::mat &m_beta = vm_theta(1+i);
        m_beta.set_size(mm_Y(1).cols(),mm_Y(1).rows()); //attention Y est un vecteur colonne donc beta un vecteur ligne
        m_beta.zeros();
      
    }
    
     
    // On remplace les nan par un valeure arbitraire (0)
    for(int i=0;i<m_pi.rows();i++)
        for(int j=0;j<m_pi.cols();j++)
            if(isnan(m_pi(i,j)))
                m_pi(i,j)=0;
   
    
    

    // Smooting
    borner(m_pi,-10,10);
   
    return vm_theta;  


}



void wmixnet::f_E_BI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();
    //nb cov
    int p= mm_Y(1).rows(); 
    int Q=m_tau.cols();

    // tout d'abord nommons les choses par leur nom.
    const itpp::mat &m_pi = vm_theta(0);

    
    m_tau2 = repmat(log(v_alpha),n,1,true);

 
    for(int i=0;i<m_tau2.rows();i++)
    {
        for(int q=0;q<m_tau2.cols();q++)
        {
            for(int j=0;j<m_tau2.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau2.cols();l++)
                    {
                        double f=0;
                        const itpp::mat &m_beta = vm_theta(1+Q*q+l);
                        double beta1=(m_beta*mm_Y(i,j))(0);
                        double beta2=(m_beta*mm_Y(j,i))(0);
                        f+=m_X(i,j)*(m_pi(q,l)+beta1)- log(1+exp(m_pi(q,l)+beta1));
                        f+=m_X(j,i)*(m_pi(l,q)+beta2)- log(1+exp(m_pi(l,q)+beta2));
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever


  

    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);
    
           
   
    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));
  
}


//fonction pour calculer la norme entre 2 jeux de parametre pour une combinaison q*l
inline double norm_paraql(const itpp::vec & v_para1,const itpp::vec & v_para2)
{
    double diff = itpp::max(abs(v_para1-v_para2));
    
    return diff;

}


void wmixnet::f_M_BI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{
    int Q=m_tau.cols();
    int n=m_X.rows();
    vm_theta2 = vm_theta;
    itpp::mat &m_pi = vm_theta2(0);
    int p= mm_Y(1).rows();
    
  // std::cerr << m_tau << std::endl;

    for (int q=0;q<Q;q++)
    {   std::cerr << q << std::endl;
        for (int l=0;l<Q;l++)
        {
             double &pi=vm_theta2(0)(q,l); 
             itpp::mat &m_beta = vm_theta2(1+Q*q+l);        
           
             itpp::vec v_para=itpp::concat(pi,m_beta.get_row(0));
             
            
            // std::cerr << v_para << std::endl;
             
             double norm_pas_thetaql=INFINITY;
             itpp::mat m_I(1+p,1+p); //matrice d'info de Fisher de taille (1+p)^2
             itpp::vec v_U(1+p); //vecteur de score de taille 1+p (p=nb de cov)
             
             do
             {    

                  v_U.zeros();
                  m_I.zeros(); 
                 
                  itpp::vec v_para_old=v_para; 
                  
                  for (int i=0;i<n;i++)
                  {    
                      for (int j=0;j<n;j++)
                      {
                          if(i!=j)
                          {    
                              
                              double machin=1;
                              itpp::vec v_cov=itpp::concat(machin,mm_Y(i,j).get_col(0));  //vecteur pour calculer la matrice d'info de fisher et le vecteur de score 
                              
                            

                              double g=pi+(m_beta*mm_Y(i,j))(0);
                           
                              double prob=1/(1+exp(-g));
                            

                              v_U+=m_tau(i,q)*m_tau(j,l)*v_cov*(m_X(i,j)-prob);
                          
                              m_I+=m_tau(i,q)*m_tau(j,l)*(itpp::repmat(v_cov,1,1,true).T()*itpp::repmat(v_cov,1,1).T()*prob*(1-prob));
                            
                             
                           }
                       }
                  
                  }
             
            
             //mise a jour de pi et des beta
             v_para+=ls_solve(m_I,v_U);  
             
             pi=v_para(0);
          
             
             m_beta=itpp::repmat(v_para(1,p),1,1,true);
          
             //calcul de la modif 
             norm_pas_thetaql=norm_paraql(v_para,v_para_old);

             } while(norm_pas_thetaql>tolEM);
         }
     }

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_pi.rows();i++)
        for(int j=0;j<m_pi.cols();j++)
            if(isnan(m_pi(i,j)))
                m_pi(i,j)=0;

   //borner les beta
   for(int k=0;k<Q*Q;k++)
    {
        itpp::mat &m_beta = vm_theta2(1+k);
        
            borner(m_beta,-30,30);
    }

     borner(m_pi,-30,30);
  
  
    
   for(int k=0;k<Q*Q;k++)
    {
        itpp::mat &m_beta = vm_theta2(1+k);
        for(int i=0;i<m_beta.rows();i++)
        for(int j=0;j<m_beta.cols();j++)
            if(isnan(m_beta(i,j)))
               m_beta(i,j)=0;
      
    }



}




double wmixnet::f_norm_theta_BI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_pi1 = vm_theta1(0);
    const itpp::mat & m_pi2 = vm_theta2(0);
   
    double diff = 2*itpp::max(itpp::max(abs(m_pi1-m_pi2)));
  
    for(int i=1;i<vm_theta1.size();i++)
    {
        const itpp::mat & m_beta1 = vm_theta1(i);
        const itpp::mat & m_beta2 = vm_theta2(i);
        diff+= 2*itpp::max(itpp::max(abs(m_beta1-m_beta2)));
    }
   
    return diff;


}


double wmixnet::f_norm2_classe_BI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::vec v_diff = m_pi.get_col(q)-m_pi.get_col(l);
    itpp::vec v_diffr = m_pi.get_row(q)-m_pi.get_row(l);

    int Q=m_pi.cols(); 
     
    double diff = elem_mult_sum(v_diff,v_diff);
    diff+=elem_mult_sum(v_diffr,v_diffr);
    
    for (int i=0;i<Q;i++)
    {
      
       itpp::vec v_diffbeta=vm_theta(i*Q+q+1).get_row(0)-vm_theta(i*Q+l+1).get_row(0);
       itpp::vec v_diffbetar=vm_theta(q*Q+i+1).get_row(0)-vm_theta(l*Q+i+1).get_row(0);
          
       diff+=elem_mult_sum(v_diffbeta,v_diffbeta); 
       diff+=elem_mult_sum(v_diffbetar,v_diffbetar); 

    } 
    return diff;
}



double wmixnet::f_PL_BI(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));
       
    int Q=m_tau.cols();
  
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        double f=0;
                        const itpp::mat &m_beta = vm_theta(1+Q*q+l);
                        double beta1=(m_beta*mm_Y(i,j))(0);
                        
                        f+=m_X(i,j)*(m_pi(q,l)+beta1)- log(1+exp(m_pi(q,l)+beta1));
                        
                        PL += f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever

    return PL;
}





int wmixnet::f_nb_parameters_BI(int Q)
{
    int p= mm_Y(1).rows();

    if(sym_done)
        return (Q*(Q+1)/2*(1+p));
    else
        return (Q*Q*(1+p));

      
}



itpp::mat wmixnet::f_residuelle_BI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::mat m_residuelle = m_X - m_tau*m_pi*m_tau.T();

    return m_residuelle;
}
