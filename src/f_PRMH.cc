// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


itpp::Vec<itpp::mat> wmixnet::f_init_PRMH(const itpp::mat &m_tau)
{
    itpp::Vec<itpp::mat> vm_theta(2);

    itpp::mat &m_lambda = vm_theta(0);
    itpp::mat &m_beta = vm_theta(1);


    m_lambda = itpp::elem_div(m_tau.T()*m_X*m_tau, m_tau.T() * m_nodiag *m_tau);

    m_beta.set_size(mm_Y(1).cols(),mm_Y(1).rows());
    m_beta.zeros();

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_lambda.rows();i++)
        for(int j=0;j<m_lambda.cols();j++)
            if(isnan(m_lambda(i,j)))
                m_lambda(i,j)=0;

    // Smooting
    borner(m_lambda,tolP,INFINITY);

    return vm_theta;
}

void wmixnet::f_E_PRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abort nomons les choses par leurs noms.
    const itpp::mat &m_lambda = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    

    m_tau2 = repmat(log(v_alpha),n,1,true);

//    std::cerr << m_X(0,1) << std::endl;
//    std::cerr << mm_Y(0,1) << std::endl;
//    std::cerr << m_beta << std::endl;
    for(int i=0;i<m_tau2.rows();i++)
    {
        for(int q=0;q<m_tau2.cols();q++)
        {
            for(int j=0;j<m_tau2.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau2.cols();l++)
                    {
                        double f=0;
                        f += -m_lambda(q,l)*exp((m_beta*mm_Y(i,j))(0));
                        f += m_X(i,j)*(log(m_lambda(q,l))+(m_beta*mm_Y(i,j))(0));
                        //f += -lgamma(m_X(i,j)+1);
                        if(!sym_done)
                        {
                            f += -m_lambda(l,q)*exp((m_beta*mm_Y(j,i))(0));
                            f += m_X(j,i)*(log(m_lambda(l,q))+(m_beta*mm_Y(j,i))(0));
                        }
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever

    
    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

inline itpp::mat augmenter_mat_c(const itpp::mat m_orig, const double val)
{
    itpp::mat m_dest(m_orig.rows()+1,1);
    m_dest(0)=val;
    for(int k=0;k<m_orig.rows();k++)
        m_dest(k+1)=m_orig(k);

    return m_dest;
}

inline itpp::mat augmenter_mat_l(const itpp::mat m_orig, const double val)
{
    return augmenter_mat_c(m_orig.T(),val).T();
}

void wmixnet::f_M_PRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{

    vm_theta2 = vm_theta;
    itpp::mat &m_lambda = vm_theta2(0);
    itpp::mat &m_beta = vm_theta2(1);

//    std::cout << vm_theta2 << std::endl << std::endl;
    
    double norm_pas_theta=INFINITY;

    do
    {
        itpp::Vec<itpp::mat> vm_old_theta=vm_theta2;
        // On compute m_eby
        itpp::mat m_eby(m_X.rows(),m_X.cols());
//        std::cerr << "mm_Y(0,1) " << mm_Y(0,1) << std::endl;
//        std::cerr << "m_beta " << m_beta << std::endl;
        for(int i=0;i<m_X.rows();i++)
            for(int j=0;j<m_X.cols();j++)
                if(i!=j)
                    m_eby(i,j)=exp((m_beta*mm_Y(i,j))(0));
                else
                    m_eby(i,j)=0;

//        std::cerr << "m_eby " << m_eby << std::endl;
//        std::cerr << "m_tau " << m_tau << std::endl;

//        std::cerr << "m_tau.T()*m_X*m_tau " << m_tau.T()*m_X*m_tau << std::endl;
//        std::cerr << "m_tau.T() * m_eby *m_tau " << m_tau.T() * m_eby *m_tau << std::endl;

        m_lambda = itpp::elem_div(m_tau.T()*m_X*m_tau, m_tau.T() * m_eby *m_tau);
//        std::cerr << "m_lambda " << m_lambda << std::endl;
        
        // On compute beta
//        std::cout << m_beta << std::endl;
        // m_A m_B
        itpp::mat m_A = m_tau*m_lambda*m_tau.T();
        itpp::mat onesQQ(m_tau.cols(),m_tau.cols());
        onesQQ.ones();
        itpp::mat m_B = m_tau*onesQQ*m_tau.T();
        itpp::mat m_beta_a = augmenter_mat_l(m_beta,0);

        itpp::mat m_2rS(m_beta.cols()+1,m_beta.cols()+1);
        m_2rS.zeros();
        for(int k=0;k<m_2rS.rows(); k++)
            m_2rS(k,k) = 2. * penality;

        double norm_pas = INFINITY;
        do
        {

            itpp::mat m_grad(m_beta_a.cols(),1);
            itpp::mat m_jacobien(m_beta_a.cols(),m_beta_a.cols());
            m_grad.zeros();
            m_jacobien.zeros();
            for(int i=0;i<m_X.rows();i++)
            {
                for(int j=0;j<m_X.cols();j++)
                {
                    if(i!=j)
                    {
                        itpp::mat m_Yij_a=augmenter_mat_c(mm_Y(i,j),1);
                        m_grad+= (
                                -m_A(i,j)*exp((m_beta_a*m_Yij_a)(0))
                                +m_X(i,j)*m_B(i,j)
                                 )*m_Yij_a - m_2rS*m_beta_a.T();
                        m_jacobien+= -m_A(i,j)*exp((m_beta_a*m_Yij_a)(0))
                                    *m_Yij_a*m_Yij_a.T() - m_2rS;
                    }
                }
            }
            itpp::mat m_pas;
            bool success = ls_solve(m_jacobien,m_grad,m_pas); // Maximisation
//            std::cerr << "Jac "<< m_jacobien << "Grad " << m_grad << "success " << success << std::endl;;
            if(success)
            {
            m_pas*=-1;
            for(int z=0;z<m_pas.size();z++)
                if(isnan(m_pas(z)))
                    m_pas(z)=0;
            m_beta_a += m_pas.T();

                norm_pas = itpp::norm(m_pas);
            }
            else
                norm_pas = 0;
            

            //std::cout << "NR " << m_beta_a << std::endl;
        } while(norm_pas>tolEM);

        for(int k=0;k<m_beta.cols();k++)
            m_beta(k)=m_beta_a(k+1);
        norm_pas_theta = f_norm_theta_PRMH(vm_old_theta,vm_theta2);
//        std::cout << norm_pas_theta << std::endl;
    } while(norm_pas_theta>tolEM);




//    std::cout << vm_theta2 << std::endl << std::endl;


    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_lambda.rows();i++)
        for(int j=0;j<m_lambda.cols();j++)
            if(isnan(m_lambda(i,j)))
                m_lambda(i,j)=0;

    
    //struct fonction_a_minimiser fun;
    //struct fonction_distance distfun;
    //fun.p_obj = this;
    //fun.pm_tau = &m_tau;
    //distfun.p_obj=this;
    //vm_theta2 = nelder_mead_simplex_algorithm(fun,vm_theta,distfun,1e-3);

   
    
}

double wmixnet::f_norm_theta_PRMH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_lambda1 = vm_theta1(0);
    const itpp::mat & m_lambda2 = vm_theta2(0);
    const itpp::mat & m_beta1 = vm_theta1(1);
    const itpp::mat & m_beta2 = vm_theta2(1);

    //double diff = 2*itpp::max(itpp::max(elem_div(abs(m_lambda1-m_lambda2),m_lambda1+m_lambda2)));
    double diff = 2*itpp::max(itpp::max(abs(itpp::log(m_lambda1)-itpp::log(m_lambda2))));
    diff+= 2*itpp::max(itpp::max(abs(m_beta1-m_beta2)));

    /*std::cerr << vm_theta1.size() << " / " << vm_theta2.size() << " / " <<m_lambda1.size() << " / "<< m_lambda2.size() << " / " << m_beta1.size() << " / " << m_beta2.size() << " / " << diff << std::endl;
    std::cerr << m_lambda1 << std::endl;
    std::cerr << m_beta1 << std::endl;
    std::cerr << m_lambda2 << std::endl;
    std::cerr << m_beta2 << std::endl;*/

    return diff;
}

double wmixnet::f_norm2_classe_PRMH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_lambda = vm_theta(0);

    itpp::vec v_diff = itpp::log(m_lambda.get_col(q))-itpp::log(m_lambda.get_col(l));
    itpp::vec v_diffr = itpp::log(m_lambda.get_row(q))-itpp::log(m_lambda.get_row(l));

    double diff = elem_mult_sum(v_diff,v_diff);
    diff += elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_PRMH(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    itpp::mat m_lambda = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    
    double mult = (sym_done) ? .5 : 1;
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        double f=0;
                        f += -m_lambda(q,l)*exp((m_beta*mm_Y(i,j))(0));
                        f += m_X(i,j)*log(m_lambda(q,l));
                        f += m_X(i,j)*(m_beta*mm_Y(i,j))(0);
                        f += -lgamma(m_X(i,j)+1);

                        PL += mult*f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever


    return PL;
}

int wmixnet::f_nb_parameters_PRMH(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2 +mm_Y(0,1).rows());
    else
        return (Q*Q +mm_Y(0,1).rows());
}

itpp::mat wmixnet::f_residuelle_PRMH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_lambda = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    
    itpp::mat m_eby(m_X.rows(),m_X.cols());
    for(int i=0;i<m_X.rows();i++)
        for(int j=0;j<m_X.cols();j++)
            if(i!=j)
                m_eby(i,j)=exp((m_beta*mm_Y(i,j))(0));
            else
                m_eby(i,j)=0;

    itpp::mat m_residuelle = m_X - itpp::elem_mult(m_tau*m_lambda*m_tau.T(),m_eby);
    return m_residuelle;
}

