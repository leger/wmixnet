// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


itpp::Vec<itpp::mat> wmixnet::f_init_GRMH(const itpp::mat &m_tau)
{

    itpp::Vec<itpp::mat> vm_theta(3);

    itpp::mat &m_mu = vm_theta(0);
    itpp::mat m_A = m_tau.T() *m_Xzd *m_tau;
    itpp::mat m_B = m_tau.T() *m_nodiag *m_tau;
    m_mu = itpp::elem_div(m_A, m_B);

    itpp::mat &m_beta = vm_theta(1);
    m_beta.set_size(mm_Y(1).rows(),1);
    m_beta.zeros();

    vm_theta(2).set_size(1,1);
    double &sigma2 = vm_theta(2)(0);

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_mu.rows();i++)
        for(int j=0;j<m_mu.cols();j++)
            if(isnan(m_mu(i,j)))
                m_mu(i,j)=0;

    int n = m_X.rows();
    double s=0;
    s+= itpp::sumsum(m_tau.T()*m_Xzd2*m_tau);
    s+= itpp::elem_mult_sum(itpp::elem_mult(m_mu,m_mu),m_B);
    s+= -2*itpp::elem_mult_sum(m_mu, m_A);
    s/= n*(n-1);

    sigma2=s;

    return vm_theta;
}

inline double lfphi(double x,double m, double s2)
{
    return(-0.5*log(2*M_PI*s2)-0.5*(x-m)*(x-m)/s2);
}
    

void wmixnet::f_E_GRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abort nomons les choses par leurs noms.
    const itpp::mat &m_mu = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    const double &sigma2 = vm_theta(2)(0);
    

    m_tau2 = repmat(log(v_alpha),n,1,true);

//    std::cerr << m_X(0,1) << std::endl;
//    std::cerr << mm_Y(0,1) << std::endl;
//    std::cerr << m_beta << std::endl;
    for(int i=0;i<m_tau2.rows();i++)
    {
        for(int q=0;q<m_tau2.cols();q++)
        {
            for(int j=0;j<m_tau2.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau2.cols();l++)
                    {
                        double f=0;
                        f+=lfphi(m_X(i,j),m_mu(q,l)+(m_beta.T()*mm_Y(i,j))(0),sigma2);
                        if(!sym_done)
                            f+=lfphi(m_X(j,i),m_mu(l,q)+(m_beta.T()*mm_Y(j,i))(0),sigma2);
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever

    
    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

void wmixnet::f_M_GRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{

    vm_theta2 = vm_theta;
    itpp::mat &m_mu = vm_theta2(0);
    itpp::mat &m_beta = vm_theta2(1);
    double &sigma2 = vm_theta2(2)(0);

//    std::cout << vm_theta2 << std::endl << std::endl;
    
    // Construction de Wql
    itpp::Mat<itpp::mat> mm_W(m_tau.cols(),m_tau.cols());
    itpp::mat m_sW(m_tau.cols(),m_tau.cols());
    for(int q=0;q<m_tau.cols();q++)
    {
        for(int l=0;l<m_tau.cols();l++)
        {
            mm_W(q,l).set_size(m_X.rows()*(m_X.rows()-1),1);
            m_sW(q,l)=0;
            int k=0;
            for(int i=0;i<m_X.rows();i++)
            {
                for(int j=0;j<m_X.rows();j++)
                {
                    if(i!=j)
                    {
                        mm_W(q,l)(k,0)=m_tau(i,q)*m_tau(j,l);
                        m_sW(q,l) += mm_W(q,l)(k,0);
                        k++;
                    }
                }
            }
        }
    }



    double norm_pas_theta=INFINITY;

    do
    {
        itpp::Vec<itpp::mat> vm_old_theta=vm_theta2;

        for(int q=0;q<m_tau.cols();q++)
            for(int l=0;l<m_tau.cols();l++)
            {
                //m_mu(q,l) = (mm_W(q,l).T()*(m_U-m_V*m_beta))(0)/m_sW(q,l);
                double &a=m_mu(q,l);
                double b= (mm_W(q,l).T()*(m_U-m_V*m_beta))(0);
                double c=m_sW(q,l);
                a=b/c;
            }

        itpp::mat m_Uem(m_U.rows(),1);
        m_Uem.zeros();
        for(int q=0;q<m_tau.cols();q++)
            for(int l=0;l<m_tau.cols();l++)
                m_Uem += m_mu(q,l)*mm_W(q,l);

        m_beta = m_T * (m_U - m_Uem);

        itpp::mat m_D=m_U-m_V*m_beta-m_Uem;
        sigma2 = itpp::elem_mult_sum(m_D,m_D)/(m_D.rows());


        

        norm_pas_theta = f_norm_theta_GRMH(vm_old_theta,vm_theta2);
//        std::cout << norm_pas_theta << std::endl;
    } while(norm_pas_theta>tolEM);




//    std::cout << vm_theta2 << std::endl << std::endl;

    
}

double wmixnet::f_norm_theta_GRMH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat &m_mu1 = vm_theta1(0);
    const itpp::mat &m_beta1 = vm_theta1(1);
    const double &sigma21 = vm_theta1(2)(0);
    
    const itpp::mat &m_mu2 = vm_theta2(0);
    const itpp::mat &m_beta2 = vm_theta2(1);
    const double &sigma22 = vm_theta2(2)(0);
    

    //double diff = 2*itpp::max(itpp::max(elem_div(abs(m_lambda1-m_lambda2),m_lambda1+m_lambda2)));
    double diff = 2*itpp::max(itpp::max(abs(m_mu1-m_mu2)));
    diff+= 2*itpp::max(itpp::max(abs(m_beta1-m_beta2)));
    diff+= 2*sqrt(abs(sigma22-sigma21));

    /*std::cerr << vm_theta1.size() << " / " << vm_theta2.size() << " / " <<m_lambda1.size() << " / "<< m_lambda2.size() << " / " << m_beta1.size() << " / " << m_beta2.size() << " / " << diff << std::endl;
    std::cerr << m_lambda1 << std::endl;
    std::cerr << m_beta1 << std::endl;
    std::cerr << m_lambda2 << std::endl;
    std::cerr << m_beta2 << std::endl;*/

    return diff;
}

double wmixnet::f_norm2_classe_GRMH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_mu = vm_theta(0);

    itpp::vec v_diff = m_mu.get_col(q)-m_mu.get_col(l);
    itpp::vec v_diffr = m_mu.get_col(l)-m_mu.get_col(q);

    double diff = elem_mult_sum(v_diff,v_diff);
    diff += elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_GRMH(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{

    //TODO
    const itpp::mat &m_mu = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    const double &sigma2 = vm_theta(2)(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    
    double mult = (sym_done) ? .5 : 1;
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        double f=lfphi(m_X(i,j),m_mu(q,l)+(m_beta.T()*mm_Y(i,j))(0),sigma2);
                        PL += mult*f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever


    return PL;
}

int wmixnet::f_nb_parameters_GRMH(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2 +mm_Y(0,1).rows()+1);
    else
        return (Q*Q +mm_Y(0,1).rows()+1);
}

itpp::mat wmixnet::f_residuelle_GRMH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_mu = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    
    itpp::mat m_prediction(m_tau.rows(),m_tau.rows());
    m_prediction.zeros();

    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        m_prediction(i,j)+=(m_mu(q,l)+(m_beta.T()*mm_Y(i,j))(0))*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    }
    
    itpp::mat m_residuelle = m_X - m_tau*m_mu*m_tau.T();
    return m_residuelle;
}

