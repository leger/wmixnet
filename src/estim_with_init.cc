// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "main.h"

int main(int argc, char **argv)
{
    itpp::RNG_randomize();
    int c;
    int symetric = false;
    std::string input="";
    int Qmin=0;
    int Qmax=0;
    std::string modele="";
    std::string smoothing_mode="";
    std::string output="";
    std::string format="";
    std::string soptname;
    std::string state_file="";
    std::string init_file="";
    double tolF=0;
    double tolP=0;
    double penality=0;
    double tolEM=0;
    bool ortho=false;
    int number_of_nodes = 0;
    int parallel=boost::thread::hardware_concurrency();

    while (1)
    {
        static struct option long_options[] =
        {
            {"symetric", no_argument,       0, 's'},
            {"symmetric", no_argument,       0, 's'},
            {"input",    required_argument, 0, 'i'},
            {"model",    required_argument, 0, 'm'},
            {"tolP",required_argument,0,0},
            {"tolF",required_argument,0,0},
            {"tolEM",required_argument,0,0},
            {"penality",required_argument,0,0},
            {"penalty",required_argument,0,0},
            {"output",required_argument,0,'o'},
            {"output-format",required_argument,0,'F'},
            {"state-file",required_argument,0,'f'},
            {"number-of-nodes",required_argument,0,'N'},
            {"ortho",no_argument,0,0},
            {"version",no_argument,0,'V'},
            {"init",required_argument,0,0},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "si:m:o:f:F:N:V",
                long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c)
        {
            case 0:
                /* If this option set a flag, do nothing else now. */
                if (long_options[option_index].flag != 0)
                    break;

                soptname=long_options[option_index].name;
                if(soptname=="tolP")
                {
                    std::cerr << "1a\n";
                    tolP = atof(optarg);
                    std::cerr << "1b\n";
                }
                if(soptname=="tolF")
                {
                    std::cerr << "2a\n";
                    tolF = atof(optarg);
                    std::cerr << "2b\n";
                }
                if(soptname=="tolEM")
                {
                    std::cerr << "3a\n";
                    tolEM = atof(optarg);
                    std::cerr << "3c\n";
                }
                if(soptname=="penality")
                {
                    std::cerr << "4a\n";
                    penality = atof(optarg);
                    std::cerr << "4b\n";
                }
                if(soptname=="penalty")
                {
                    std::cerr << "5a\n";
                    penality = atof(optarg);
                    std::cerr << "5b\n";
                }
                if(soptname=="ortho")
                {
                    std::cerr << "6a\n";
                    ortho = true;
                    std::cerr << "6b\n";
                }
                if(soptname=="init")
                {
                    std::cerr << "7a\n";
                    init_file = optarg;
                    std::cerr << "7b\n";
                }
                break;


            case 's':
                symetric = true;
                break;

            case 'i':
                input = optarg;
                break;

            case 'm':
                modele = optarg;
                break;

            case 'o':
                output = optarg;
                break;

            case 'F':
                format = optarg;
                break;
            
            case 'f':
                state_file = optarg;
                break;

            case 'N':
                number_of_nodes = atoi(optarg);
                break;

            case 'V':
                std::cerr << PACKAGE_STRING << std::endl;
                return(0);
                break;

            case '?':
                /* getopt_long already printed an error message. */
                abort();
                break;

            default:
                abort ();
        }
    }

    std::cerr << 1 << std::endl;

    Qmin = 1;

    wmixnet wmix;

    // si ni state_file, ni input est defini il y a un pb
    if(input=="" && state_file=="")
    {
        std::cerr << "input file or state file must be defined" << std::endl;
        abort();
    }

    itpp::mat m_tau_init;

    std::ifstream ifs(init_file.c_str());

    if(ifs.is_open())
    {
        boost::iostreams::filtering_istream in;
        in.push(boost::iostreams::gzip_decompressor());
        in.push(ifs);
        boost::archive::binary_iarchive ia(in);
        ia >> m_tau_init;
    }

    std::cout << m_tau_init << std::endl;

    Qmax = m_tau_init.cols();
    Qmin=Qmax;

    wmix.set_QminQmax(Qmin,Qmax);


    // si le state_file n'est pas defini on le positionne a input+".state"
    if(state_file == "")
    {
        state_file = input+".state";
    }

    // First we load wmix from eventually previous
    wmix.set_state_file(state_file);
    wmix.load_state();

    if(tolF>0)
        wmix.set_tolF(tolF);
    if(tolP>0)
        wmix.set_tolP(tolP);
    if(tolEM>0)
        wmix.set_tolEM(tolEM);
    if(penality>0)
        wmix.set_penality(penality);

    if(modele!="")
    {
        if(!wmix.model_defined)
        {
            wmix.set_modele(modele);
        }
        else
        {
            std::cerr << "You are not allowed to change model without deleting state file, model flag is ignored" << std::endl;
        }
    }


    if(output!="")
        wmix.set_output(output);

    if(format!="")
        wmix.set_format(format);

    wmix.parallel=parallel;


    // We load the input
    wmix.load_net( input , number_of_nodes );
    if(symetric)
        wmix.symetriser();

    if(ortho)
        wmix.ortho_covariates();

    wmix.precalcul();

    wmix.em(m_tau_init,Qmax);

    wmix.save();


  
    exit (0);
}

