// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::internal_zd(itpp::mat & m_M)
{
    int n=(m_M.rows()<m_M.cols())?m_M.rows():m_M.cols();
    for(int i=0;i<n;i++)
    {
        m_M(i,i)=0;
    }

}

itpp::mat wmixnet::internal_zd_c(itpp::mat m_M)
{
    int n=(m_M.rows()<m_M.cols())?m_M.rows():m_M.cols();
    for(int i=0;i<n;i++)
    {
        m_M(i,i)=0;
    }

    return(m_M);
}

void wmixnet::borner(itpp::mat & m_M, double a, double b)
{
    for(int i=0;i<m_M.rows();i++)
    {
        for(int j=0;j<m_M.cols();j++)
        {
            if(m_M(i,j)<a)
                m_M(i,j)=a;
            if(m_M(i,j)>b)
                m_M(i,j)=b;
        }
    }
}

void wmixnet::borner(itpp::vec & v_V, double a, double b)
{
    for(int i=0;i<v_V.size();i++)
    {
        if(v_V(i)<a)
            v_V(i)=a;
        if(v_V(i)>b)
            v_V(i)=b;
    }
}

void wmixnet::writetmp(double tps)
{
    std::cerr << "[ ";

    int S = tps+.5;

    int s = S%60;
    int m = (S/60)%60;
    int h = (S/3600)%24;
    int d = (S/86400);

    if(d>0)
        std::cerr << std::setw(3) << d << "d ";

    if(h>0 || d>0)
        std::cerr << std::setw(2) << h << "h ";
    else
        std::cerr << "    ";

    if((m>0 || h>0) && d==0)
        std::cerr << std::setw(2) << m << "m ";
    else
        if(d==0)
            std::cerr << "    ";

    if(d==0)
        if(S==0)
            std::cerr << "<1s ";
        else
            std::cerr << std::setw(2) << s << "s ";
    else
        std::cerr << "   ";

    std::cerr << "]";
}

itpp::mat wmixnet::tau_to_Z(const itpp::mat & m_tau)
{
    itpp::mat m_Z;
    m_Z.set_size(m_tau.rows(),m_tau.cols());
    m_Z.zeros();

    for(int i=0;i<m_tau.rows();i++)
        m_Z(i,max_index(m_tau.get_row(i)))=1;

    return m_Z;
}
