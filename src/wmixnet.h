// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef H_WMIXNET_WMIXNET
#define H_WMIXNET_WMIXNET 1


#include <iostream>
#include <itpp/itbase.h>
#include <itpp/itstat.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <iomanip>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp> 
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>
#include <time.h>


#include "config_undef.h"
#include "config.h"


#include "itpp_serialize.h"

class wmixnet
{
    private:
    public:
        // donnees du reseau
        itpp::mat m_X;
        itpp::Mat<itpp::mat> mm_Y;
        int Qmin;
        int Qmax;

        // Variable arbitraire
        double tolP;
        double tolF;
        double tolEM;
        double penality;

        // Mutex
        boost::mutex mutex_donnees; // non archivé

        // Orthogonalisation, on fait la transfo Yij <- A*(Yij-b)
        void ortho_covariates();
        bool ortho_done;
        itpp::mat m_transfm; // A
        itpp::mat m_cov_moyennes; // b


        // Variable precalculees;
        void precalcul();
        itpp::mat m_Xzd;
        itpp::mat m_XzdT;
        itpp::mat m_X1zd;
        itpp::mat m_X1zdT;
        itpp::mat m_nodiag;
        itpp::mat m_Xzd2;
        itpp::mat m_U;
        itpp::mat m_V;
        itpp::mat m_T;
        itpp::Vec<itpp::mat> vm_precalc_model;


        // Flags
        bool model_defined;
        bool em_init;
        bool sym_done;
        int parallel; // Ce champ est volontairement pas archivé
        std::string smoothing_mode;

        std::string output;
        std::string format;
        std::string modele;
        std::string state_file;

        double exploration;

        // fonction de verification
        void verif_Q_defined();
        void verif_model_defined();
        void verif_em_with_init();

        // fonction de serialisation
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & m_X;
            ar & mm_Y;
            ar & Qmin;
            ar & Qmax;
            ar & tolP;
            ar & tolF;
            ar & tolEM;
            ar & penality;
            ar & model_defined;
            ar & em_init;
            ar & sym_done;
            ar & smoothing_mode;
            ar & output;
            ar & format;
            ar & modele;
            ar & state_file;
            ar & vv_alpha;
            ar & vm_tau;
            ar & vvm_theta;
            ar & v_J;
            ar & v_PL;
            ar & v_H;
            ar & v_ICL;
            ar & v_id;
            ar & v_last_ascend;
            ar & v_last_descend;
            ar & exploration;
            ar & m_coords_SC0;
            ar & ortho_done;
            ar & m_transfm; 
            ar & m_cov_moyennes;
            if (Archive::is_loading::value)
            {
                set_modele(modele);
                precalcul();
            }
        }


        // donnees calcules
    public:
        itpp::Vec<itpp::vec> vv_alpha;
        itpp::Vec<itpp::mat> vm_tau;
        itpp::Vec< itpp::Vec<itpp::mat> > vvm_theta;
        itpp::vec v_J;
        itpp::vec v_H;
        itpp::vec v_PL;
        itpp::vec v_ICL;
        itpp::ivec v_id;
        itpp::ivec v_last_ascend;
        itpp::ivec v_last_descend;
        itpp::mat m_coords_SC0;

        // Methodes internes
        itpp::Vec<itpp::ivec> spectral_clustering_kmeans(const itpp::mat & m_Coords, const itpp::ivec & v_indexes, int dim, int k);
        itpp::mat spectral_clustering_coords(const itpp::mat & m_A);
        itpp::mat construire_init(int Q);

        // Tanbouille
        static void writetmp(double);
        static void internal_zd(itpp::mat & m_M);
        static itpp::mat internal_zd_c(itpp::mat m_M);
        static void borner(itpp::mat &, double, double);
        static void borner(itpp::vec &, double, double);
        static itpp::mat tau_to_Z(const itpp::mat &);
        struct job_em_init;
        struct job_smoothing_ascend;
        struct job_smoothing_descend;

        // Fonctions des modeles
        //
        // Bernoulli
        itpp::Vec<itpp::mat> f_init_bernoulli(const itpp::mat &m_tau);
        void f_E_bernoulli( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_bernoulli( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_bernoulli(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_bernoulli(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_bernoulli( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_bernoulli(int Q);
        itpp::mat f_residuelle_bernoulli(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);


        // BI
        itpp::Vec<itpp::mat> f_init_BI(const itpp::mat &m_tau);
        void f_E_BI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_BI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_BI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_BI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_BI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_BI(int Q);
        itpp::mat f_residuelle_BI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);


        // BH
        itpp::Vec<itpp::mat> f_init_BH(const itpp::mat &m_tau);
        void f_E_BH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_BH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_BH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_BH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_BH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_BH(int Q);
        itpp::mat f_residuelle_BH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);


        // Poisson
        itpp::Vec<itpp::mat> f_init_poisson(const itpp::mat &m_tau);
        void f_E_poisson( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_poisson( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_poisson(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_poisson(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_poisson( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_poisson(int Q);
        itpp::mat f_residuelle_poisson(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // Gaussian
        itpp::Vec<itpp::mat> f_init_gaussian(const itpp::mat &m_tau);
        void f_E_gaussian( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_gaussian( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_gaussian(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_gaussian(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_gaussian( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_gaussian(int Q);
        itpp::mat f_residuelle_gaussian(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // PRMH
        itpp::Vec<itpp::mat> f_init_PRMH(const itpp::mat &m_tau);
        void f_E_PRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_PRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_PRMH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_PRMH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_PRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_PRMH(int Q);
        itpp::mat f_residuelle_PRMH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // PRMI
        itpp::Vec<itpp::mat> f_init_PRMI(const itpp::mat &m_tau);
        void f_E_PRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_PRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_PRMI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_PRMI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_PRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_PRMI(int Q);
        itpp::mat f_residuelle_PRMI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // GRMH
        itpp::Vec<itpp::mat> f_init_GRMH(const itpp::mat &m_tau);
        void f_E_GRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_GRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_GRMH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_GRMH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_GRMH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_GRMH(int Q);
        itpp::mat f_residuelle_GRMH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);
        
        // GRMI
        itpp::Vec<itpp::mat> f_init_GRMI(const itpp::mat &m_tau);
        void f_E_GRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2);
        void f_M_GRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 );
        double f_norm_theta_GRMI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2);
        double f_norm2_classe_GRMI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l);
        double f_PL_GRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta );
        int f_nb_parameters_GRMI(int Q);
        itpp::mat f_residuelle_GRMI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // fonctions communes
        void f_precalcul_nothing(){}

        // Pointeurs vers les fonctions du modeles
        void (wmixnet::*p_f_precalcul_modele) ();
        itpp::Vec<itpp::mat> (wmixnet::*p_f_init_modele) (const itpp::mat &);
        void (wmixnet::*p_f_E_modele) ( const itpp::vec &, const itpp::mat &, const itpp::Vec<itpp::mat>, itpp::mat &);
        void (wmixnet::*p_f_M_modele) ( const itpp::vec &, const itpp::mat &, const itpp::Vec<itpp::mat> & , itpp::Vec<itpp::mat> &);
        double (wmixnet::*p_f_norm_theta_modele) (const itpp::Vec<itpp::mat> &, const itpp::Vec<itpp::mat> &);
        double (wmixnet::*p_f_norm2_classe_modele) (const itpp::Vec<itpp::mat> &, int, int);
        double (wmixnet::*p_f_PL_modele) ( const itpp::vec &, const itpp::mat &, const itpp::Vec<itpp::mat> & );
        int (wmixnet::*p_f_nb_parameters_modele) ( int );
        itpp::mat (wmixnet::*p_f_residuelle_modele) (const itpp::mat &, const itpp::Vec<itpp::mat> &);

    public:
        // Fonctions avec abstraction du modele
        void f_precalcul_modele() { return (this->*p_f_precalcul_modele)(); }
        itpp::Vec<itpp::mat> f_init_modele(const itpp::mat &m_tau) { return (this->*p_f_init_modele)(m_tau); }
        void f_E_modele(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2) { (this->*p_f_E_modele)( v_alpha, m_tau, vm_theta, m_tau2); }
        void f_M_modele(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2 ) { (this->*p_f_M_modele)( v_alpha, m_tau, vm_theta, vm_theta2 ); }
        double f_norm_theta_modele(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2) {return (this->*p_f_norm_theta_modele)(vm_theta1,vm_theta2);}
        double f_norm2_classe_modele(const itpp::Vec<itpp::mat> & vm_theta,int q, int l) {return (this->*p_f_norm2_classe_modele)(vm_theta,q,l);}
        double f_PL_modele(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta ) {return (this->*p_f_PL_modele)( v_alpha, m_tau, vm_theta ); }
        int f_nb_parameters_modele(int Q) { return (this->*p_f_nb_parameters_modele)(Q); }
        itpp::mat f_residuelle_modele(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta) { return (this->*p_f_residuelle_modele)( m_tau, vm_theta);}

    public:
        // constructeurs. Il n'y a pas de constructeur par recopie, c'est
        // normal.
        wmixnet();
        void load_net( std::string input , int);
        void load_net( itpp::mat m_X_i, itpp::Mat<itpp::vec> mm_Y_i );

        // getters
        int get_Qmin();
        int get_Qmax();
        itpp::mat get_m_X();
        itpp::Mat<itpp::mat> get_mm_Y();
        std::string get_modele() {return modele;}

        // setters
        void set_QminQmax(int Qmin_i,int Qmax_i);
        void set_tolP(double);
        void set_tolF(double);
        void set_tolEM(double);
        void set_penality(double);
        void set_exploration(double);
        void set_smoothing_mode(std::string);
        void set_output(std::string);
        void set_format(std::string);
        void set_state_file(std::string);



        void init_vresults();

        // methodes
        void set_modele(const std::string & s_modele);
        void symetriser();


        double calc_H(const itpp::mat & m_tau);
        double calc_J(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);


        // E step
        itpp::mat e_step(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta);

        // EM
        void do_first();
        void em(itpp::mat m_tau, int Q);
        void em_with_init();

        // Smoothing
        bool smoothing_ascend(int Q);
        bool smoothing_descend(int Q);
        itpp::bvec smoothing_pbs();
        void smoothing();

        // Automatic
        void automatic();

        // State et results
        void save_state();
        void load_state();
        void save();




        template <class T, class T2>
        static itpp::Vec<itpp::mat> nelder_mead_simplex_algorithm(T & fun,const itpp::Vec<itpp::mat> & vm_theta, T2 & distfun, double epsilon);






};



#endif
