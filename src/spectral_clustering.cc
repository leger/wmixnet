// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

itpp::mat wmixnet::spectral_clustering_coords(const itpp::mat & m_A)
{
    // m_A : matrice d'adjacence
    
    // Premierement on normalise la matrice et on calcule l'exponnentielle
    double moyenne=itpp::sumsum(m_A)/(m_A.rows()*m_A.cols());
    itpp::mat m_B=m_A-moyenne;
    double variance=itpp::elem_mult_sum(m_B,m_B)/(m_A.rows()*m_A.cols()-1);

    double std = sqrt(variance);

    // Ca devrait marcher suivant la doc d'itpp...
    //itpp::mat m_E = 1.0/(1+itpp::exp((-1/std)*m_B));
    //Contournement :
    itpp::mat m_E(m_A.rows(),m_A.cols());
    for(int i=0;i<m_A.rows();i++)
        for(int j=0;j<m_A.cols();j++)
            m_E(i,j) = 1.0/(1.0+exp(-m_B(i,j)/std));

    // Maintenant on fait du spectral clustering sur m_E
    itpp::vec v_sDi = itpp::sqrt(itpp::sum(m_E,1));
    itpp::vec v_sDo = itpp::sqrt(itpp::sum(m_E,2));

    // On crée le Laplacien normalisé (Attention graph non sym)
    itpp::mat m_L(m_E.rows(),m_E.cols());

    for(int i=0;i<m_E.rows();i++)
        for(int j=0;j<m_E.cols();j++)
            m_L(i,j)=m_E(i,j)/(v_sDi(j)*v_sDo(i));

    itpp::mat m_L2 = m_L*m_L.T()+m_L.T()*m_L;

    itpp::mat m_P;
    itpp::vec v_lambda;
    
    if(!itpp::eig_sym(m_L2,v_lambda,m_P))
    {
        std::cerr << "Matrice non diagonalisable" << std::endl;
        abort();
    }

    // Tri dans l'ordre croissant
    itpp::ivec v_index_lambda = itpp::sort_index(v_lambda);

//    itpp::mat m_Coords = m_P.get_cols(v_index_lambda.get(v_lambda.length()-k2,v_lambda.length()-1));
    itpp::mat m_Coords = m_P.get_cols(v_index_lambda);

    return m_Coords;
}




    



    



itpp::Vec<itpp::ivec> wmixnet::spectral_clustering_kmeans(const itpp::mat & m_Coords, const itpp::ivec & v_indexes, int dim, int k)
{
    // m_Coords : Coordonnees
    // v_indexes : noeuds à considerer
    // k : nombre de groupe à faire
    // dim : nombre de dimentions à considerer
    //
    // sortie : vecteur parametré par le numero du groupe, dont chaque
    // composante est un ivec contenant les indices

    if(k<2)
    {
        itpp::Vec<itpp::ivec> vv_sortie;
        vv_sortie(0)=v_indexes;
        return vv_sortie;
    }

    if(dim>m_Coords.cols())
        abort();
    
    itpp::Vec<itpp::ivec> vv_sortie(k);

    itpp::ivec v_init_centroids(k);
    itpp::ivec v_indexes_init(v_indexes);


    // On compute la matrice des distances des noeuds à considerer
    itpp::mat m_dists2(v_indexes.length(),v_indexes.length());

    //diagonale
    for(int i=0;i<v_indexes.length();i++)
        m_dists2(i,i)=0;

    // triangle superieur
    for(int i=0;i<v_indexes.length();i++)
    {
        for(int j=i+1;j<v_indexes.length();j++)
        {
            double &d2 = m_dists2(i,j);
            d2=0;
            for(int t=0;t<dim;t++)
            {
                double d=m_Coords(v_indexes(i),m_Coords.cols()-t-1)-m_Coords(v_indexes(j),m_Coords.cols()-t-1);
                d2+=d*d;
            }
        }
    }

    //t inf
    for(int i=0;i<v_indexes.length();i++)
        for(int j=0;j<i;j++)
            m_dists2(i,j)=m_dists2(j,i);

    // On trouve les deux premiers
    itpp::ivec v_index_max;
    itpp::vec v_max = itpp::max(m_dists2,v_index_max);

    int index_1;
    itpp::max(v_max,index_1);
    int index_2=v_index_max(index_1);

    itpp::ivec v_centroids_init(k);
    v_centroids_init(0)=index_1;
    for(int l=1;l<k;l++)
        v_centroids_init(l)=index_2;

    itpp::mat m_dists2_extraite = m_dists2.get_rows(v_centroids_init);

    for(int l=2;l<k;l++)
    {
        // on crée le 3e centroid et ainsi de suite
        int index;
        itpp::max(itpp::min(m_dists2_extraite),index);
        v_centroids_init(l)=index;
        m_dists2_extraite.set_row(l,m_dists2.get_row(index));
    }

    
    // ici on a un ivec v_init_centroid, contenant les noeuds d'initialisation

    itpp::mat m_Coords_centroids(k,dim);

    for(int i=0;i<k;i++)
    {
        for(int j=0;j<dim;j++)
        {
            m_Coords_centroids(i,j) = m_Coords(v_indexes(v_centroids_init(i)),m_Coords.cols()-j-1);
        }
    }

    itpp::ivec v_affect(v_indexes.length());
    v_affect.zeros();

    bool les_affectations_bougent=true;
    while(true)
    {
        les_affectations_bougent=false;

        // On fait les affectations
        for(int i=0;i<v_indexes.length();i++)
        {
            // On calcule le centroid le plus proche
            int centroid = 0;
            double dist2_min = INFINITY;

            for(int j=0;j<k;j++)
            {
                double dist2 = 0;
                for(int t=0;t<dim;t++)
                {
                    double d=m_Coords(v_indexes(i),m_Coords.cols()-t-1)-m_Coords_centroids(j,t);
                    dist2+=d*d;
                }
                if(dist2<dist2_min)
                {
                    dist2_min=dist2;
                    centroid = j;
                }
            }

            if(v_affect(i)!=centroid)
                les_affectations_bougent=true;

            v_affect(i)=centroid;
        }

        if(!les_affectations_bougent)
            break;

        // On recalcule les centroids

        m_Coords_centroids.zeros();
        itpp::vec v_nb(k);
        v_nb.zeros();

        for(int i=0;i<v_indexes.length();i++)
        {
            for(int t=0;t<dim;t++)
                m_Coords_centroids(v_affect(i),t)+=m_Coords(v_indexes(i),m_Coords.cols()-t-1);
            v_nb(v_affect(i))++;
        }

        for(int i=0;i<k;i++)
            for(int t=0;t<dim;t++)
                m_Coords_centroids(i,t)/=v_nb(i);


    }


    for(int i=0;i<v_indexes.length();i++)
    {
        vv_sortie(v_affect(i)).set_length(vv_sortie(v_affect(i)).length()+1, true);
        vv_sortie(v_affect(i))(vv_sortie(v_affect(i)).length()-1)=v_indexes(i);
    }

    return vv_sortie;
}

            



