// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


itpp::Vec<itpp::mat> wmixnet::f_init_poisson(const itpp::mat &m_tau)
{
    itpp::Vec<itpp::mat> vm_theta(1);

    itpp::mat &m_lambda = vm_theta(0);


    m_lambda = itpp::elem_div(m_tau.T()*m_X*m_tau, m_tau.T() * m_nodiag *m_tau);

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_lambda.rows();i++)
        for(int j=0;j<m_lambda.cols();j++)
            if(isnan(m_lambda(i,j)))
                m_lambda(i,j)=0;

    // Smooting
    borner(m_lambda,tolP,INFINITY);

    return vm_theta;

}

void wmixnet::f_E_poisson( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abort nomons les choses par leurs noms.
    const itpp::mat &m_lambda = vm_theta(0);
    
    m_tau2 = repmat(log(v_alpha),n,1,true);
    m_tau2 -= m_nodiag*m_tau*m_lambda.T();
    m_tau2 += m_Xzd*m_tau*log(m_lambda.T());
    if(!sym_done)
    {
        m_tau2 -= m_nodiag*m_tau*m_lambda;
        m_tau2 += m_XzdT*m_tau*log(m_lambda);
    }


    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

void wmixnet::f_M_poisson( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{
    vm_theta2.set_size(1);
    itpp::mat &m_lambda = vm_theta2(0);

    m_lambda = itpp::elem_div(m_tau.T() *m_X *m_tau, m_tau.T() *m_nodiag *m_tau);
    
    
    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_lambda.rows();i++)
        for(int j=0;j<m_lambda.cols();j++)
            if(isnan(m_lambda(i,j)))
                m_lambda(i,j)=0;


    borner(m_lambda,tolP,INFINITY);
}

double wmixnet::f_norm_theta_poisson(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_lambda1 = vm_theta1(0);
    const itpp::mat & m_lambda2 = vm_theta2(0);

    //double diff = 2*itpp::max(itpp::max(elem_div(abs(m_lambda1-m_lambda2),m_lambda1+m_lambda2)));
    double diff = 2*itpp::max(itpp::max(abs(itpp::log(m_lambda1)-itpp::log(m_lambda2))));

    return diff;
}

double wmixnet::f_norm2_classe_poisson(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_lambda = vm_theta(0);

    itpp::vec v_diff = itpp::log(m_lambda.get_col(q))-itpp::log(m_lambda.get_col(l));
    itpp::vec v_diffr = itpp::log(m_lambda.get_row(q))-itpp::log(m_lambda.get_row(l));

    double diff = itpp::elem_mult_sum(v_diff,v_diff);
    diff += itpp::elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_poisson(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_lambda = vm_theta(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    double mult = (sym_done) ? .5 : 1;
    PL-= mult*itpp::elem_mult_sum(m_lambda, m_tau.T()*m_nodiag*m_tau);
    PL+= mult*itpp::elem_mult_sum(log(m_lambda), m_tau.T()*m_X*m_tau);
    PL-= mult*itpp::elem_mult_sum(itpp::apply_function<double>(lgamma,m_X+1),m_nodiag);

    return PL;
}

int wmixnet::f_nb_parameters_poisson(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2);
    else
        return (Q*Q);
}

itpp::mat wmixnet::f_residuelle_poisson(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_lambda = vm_theta(0);
    itpp::mat m_residuelle = m_X - m_tau*m_lambda*m_tau.T();
    return m_residuelle;
}

