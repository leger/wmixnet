// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

inline itpp::mat read_beta(const itpp::Vec<itpp::mat> & vm_theta,int q, int l)
{
    int p=vm_theta.size()-1;
    itpp::mat m_beta(1,p);
    for(int i=0;i<p;i++)
        m_beta(i)=vm_theta(i+1)(q,l);
    return(m_beta);
}

inline void write_beta(itpp::Vec<itpp::mat> & vm_theta,int q, int l, const itpp::mat & m_beta)
{
    int p=vm_theta.size()-1;
    for(int i=0;i<p;i++)
        vm_theta(i+1)(q,l)=m_beta(i);
}

itpp::Vec<itpp::mat> wmixnet::f_init_PRMI(const itpp::mat &m_tau)
{
    int p=mm_Y(1).rows();
    itpp::Vec<itpp::mat> vm_theta(p+1);

    itpp::mat &m_lambda = vm_theta(0);


    m_lambda = itpp::elem_div(m_tau.T()*m_X*m_tau, m_tau.T() * m_nodiag *m_tau);

    for(int i=0;i<p;i++)
    {
        vm_theta(i+1).set_size(m_tau.cols(),m_tau.cols());
        vm_theta(i+1).zeros();
    }


    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_lambda.rows();i++)
        for(int j=0;j<m_lambda.cols();j++)
            if(isnan(m_lambda(i,j)))
                m_lambda(i,j)=0;

    // Smooting
    borner(m_lambda,tolP,INFINITY);

    return vm_theta;
}

void wmixnet::f_E_PRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abort nomons les choses par leurs noms.
    const itpp::mat &m_lambda = vm_theta(0);
    

    m_tau2 = repmat(log(v_alpha),n,1,true);

    for(int q=0;q<m_tau2.cols();q++)
    {
        for(int l=0;l<m_tau2.cols();l++)
        {
            itpp::mat m_betaql = read_beta(vm_theta,q,l);
            itpp::mat m_betalq = read_beta(vm_theta,l,q);
            for(int i=0;i<m_tau2.rows();i++)
            {
                for(int j=0;j<m_tau2.rows();j++)
                {
                    if(i!=j)
                    {
                        double f=0;
                        f += -m_lambda(q,l)*exp((m_betaql*mm_Y(i,j))(0));
                        f += m_X(i,j)*(log(m_lambda(q,l))+(m_betaql*mm_Y(i,j))(0));
                        //f += -lgamma(m_X(i,j)+1);
                        if(!sym_done)
                        {
                            f += -m_lambda(l,q)*exp((m_betaql*mm_Y(j,i))(0));
                            f += m_X(j,i)*(log(m_lambda(l,q))+(m_betalq*mm_Y(j,i))(0));
                        }
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    }

    
    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

inline itpp::mat augmenter_mat_c(const itpp::mat m_orig, const double val)
{
    itpp::mat m_dest(m_orig.rows()+1,1);
    m_dest(0)=val;
    for(int k=0;k<m_orig.rows();k++)
        m_dest(k+1)=m_orig(k);

    return m_dest;
}

inline itpp::mat augmenter_mat_l(const itpp::mat m_orig, const double val)
{
    return augmenter_mat_c(m_orig.T(),val).T();
}

void wmixnet::f_M_PRMI( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{

    vm_theta2 = vm_theta;
    itpp::mat &m_lambda = vm_theta2(0);

    
    double norm_pas_theta=INFINITY;

    itpp::Vec<itpp::mat> vm_old_theta=vm_theta2;

    itpp::mat m_2rS(vm_theta2.size(),vm_theta2.size());
    m_2rS.zeros();
    for(int k=1;k<m_2rS.rows(); k++)
        m_2rS(k,k) = 2. * penality;

    for(int q=0;q<m_tau.cols();q++)
    {
        for(int l=0;l<m_tau.cols();l++)
        {
            // Newton-Raphson sur beta_ql(augmenté)
            itpp::mat m_betaql_a = augmenter_mat_l(read_beta(vm_theta2,q,l),log(m_lambda(q,l)));

            double dpas = INFINITY;
            do
            {
                itpp::mat m_grad(m_betaql_a.cols(),1);
                itpp::mat m_hessien(m_betaql_a.cols(),m_betaql_a.cols());

                m_grad.zeros();
                m_hessien.zeros();

                for(int i=0;i<m_tau.rows();i++)
                {
                    for(int j=0;j<m_tau.rows();j++)
                    {
                        if(i!=j)
                        {
                            itpp::mat m_Yij_a = augmenter_mat_c(mm_Y(i,j),1);
                            double f=(-exp((m_betaql_a*m_Yij_a)(0))+m_X(i,j))*m_tau(i,q)*m_tau(j,l);
                            m_grad+=f*m_Yij_a;
                            m_hessien+=-exp((m_betaql_a*m_Yij_a)(0))*m_tau(i,q)*m_tau(j,l)*m_Yij_a*m_Yij_a.T();
                        }
                    }
                }

                m_grad+=-m_2rS*m_betaql_a.T();
                m_hessien+=-m_2rS;
                itpp::mat m_pas;
                if(! ls_solve_chol(-m_hessien,-m_grad,m_pas))
                {
                    std::cerr << "ls_solve echec" << std::endl;
                    break;
                }

                m_betaql_a += -m_pas.T();
                dpas = itpp::max(itpp::max(itpp::abs(m_pas)));
            } while(dpas>tolEM);

            m_lambda(q,l) = exp(m_betaql_a(0));
            write_beta(vm_theta2,q,l,m_betaql_a(0,0,1,-1));
        }
    }
        
}



double wmixnet::f_norm_theta_PRMI(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_lambda1 = vm_theta1(0);
    const itpp::mat & m_lambda2 = vm_theta2(0);

    double diff=itpp::max(itpp::max(itpp::abs(itpp::log(m_lambda1)-itpp::log(m_lambda2))));

    for(int p=1;p<vm_theta1.size();p++)
        diff+=itpp::max(itpp::max(itpp::abs(vm_theta1(p)-vm_theta2(p))));

    return diff;
}

double wmixnet::f_norm2_classe_PRMI(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_lambda = vm_theta(0);

    itpp::vec v_diff = itpp::log(m_lambda.get_col(q))-itpp::log(m_lambda.get_col(l));
    itpp::vec v_diffr = itpp::log(m_lambda.get_row(q))-itpp::log(m_lambda.get_row(l));

    double diff = elem_mult_sum(v_diff,v_diff);
    diff += elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_PRMI(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    itpp::mat m_lambda = vm_theta(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    
    double mult = (sym_done) ? .5 : 1;
    for(int q=0;q<m_tau.cols();q++)
    {
        for(int l=0;l<m_tau.cols();l++)
        {
            itpp::mat m_betaql = read_beta(vm_theta,q,l);
            for(int i=0;i<m_tau.rows();i++)
            {
                for(int j=0;j<m_tau.rows();j++)
                {
                    if(i!=j)
                    {
                        double f=0;
                        f += -m_lambda(q,l)*exp((m_betaql*mm_Y(i,j))(0));
                        f += m_X(i,j)*log(m_lambda(q,l));
                        f += m_X(i,j)*(m_betaql*mm_Y(i,j))(0);
                        f += -lgamma(m_X(i,j)+1);

                        PL += mult*f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    }


    return PL;
}

int wmixnet::f_nb_parameters_PRMI(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2*(1+mm_Y(0,1).rows()));
    else
        return (Q*Q*(1 +mm_Y(0,1).rows()));
}

itpp::mat wmixnet::f_residuelle_PRMI(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_lambda = vm_theta(0);
    
    itpp::mat m_residuelle = m_X;
    
    for(int q=0;q<m_tau.cols();q++)
    {
        for(int l=0;l<m_tau.cols();l++)
        {
            itpp::mat m_betaql = read_beta(vm_theta,q,l);
            for(int i=0;i<m_tau.rows();i++)
            {
                for(int j=0;j<m_tau.rows();j++)
                {
                    if(i!=j)
                    {
                        m_residuelle(i,j)-= m_tau(i,q)*m_tau(j,l)
                                             *m_lambda(q,l)*exp((m_betaql*mm_Y(i,j))(0));
                    }
                }
            }
        }
    }

                                                 
                                                

    return m_residuelle;
}

