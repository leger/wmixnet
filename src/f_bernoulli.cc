// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


itpp::Vec<itpp::mat> wmixnet::f_init_bernoulli(const itpp::mat &m_tau)
{
    itpp::Vec<itpp::mat> vm_theta(1);

    itpp::mat &m_pi = vm_theta(0);


    m_pi = itpp::elem_div(m_tau.T() *m_X *m_tau, m_tau.T() * m_nodiag *m_tau);

    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_pi.rows();i++)
        for(int j=0;j<m_pi.cols();j++)
            if(isnan(m_pi(i,j)))
                m_pi(i,j)=0;

    // Smooting
    borner(m_pi,tolP,1-tolP);

    return vm_theta;

}

void wmixnet::f_E_bernoulli( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abord nommons les choses par leur nom.
    const itpp::mat &m_pi = vm_theta(0);
    
    //m_tau2 = 
    //        repmat(log(v_alpha),n,1,true)
    //        + internal_zd_c(m_X)*m_tau*log(m_pi).T()
    //        + internal_zd_c(1-m_X)*m_tau*log(1-m_pi).T()
    //        ;
    m_tau2 = repmat(log(v_alpha),n,1,true);
    m_tau2+= m_Xzd*m_tau*log(m_pi).T();
    m_tau2+= m_X1zd*m_tau*log(1-m_pi).T();
    if(!sym_done)
    {
        m_tau2+= m_XzdT*m_tau*log(m_pi);
        m_tau2+= m_X1zdT*m_tau*log(1-m_pi);
    }

    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

void wmixnet::f_M_bernoulli( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{
    vm_theta2.set_size(1);
    itpp::mat &m_pi = vm_theta2(0);

    m_pi = itpp::elem_div(m_tau.T() *m_X *m_tau, m_tau.T() *m_nodiag *m_tau);
    
    
    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_pi.rows();i++)
        for(int j=0;j<m_pi.cols();j++)
            if(isnan(m_pi(i,j)))
                m_pi(i,j)=0;


    borner(m_pi,tolP,1-tolP);
}

double wmixnet::f_norm_theta_bernoulli(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_pi1 = vm_theta1(0);
    const itpp::mat & m_pi2 = vm_theta2(0);

    double diff = itpp::max(itpp::max(abs(m_pi1-m_pi2)));

    return diff;
}

double wmixnet::f_norm2_classe_bernoulli(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::vec v_diff = m_pi.get_col(q)-m_pi.get_col(l);
    itpp::vec v_diffr = m_pi.get_row(q)-m_pi.get_row(l);

    double diff = elem_mult_sum(v_diff,v_diff);
    diff+=elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_bernoulli(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    double mult = (sym_done) ? .5 : 1;
    PL+= mult * itpp::elem_mult_sum(log(m_pi)-log(1-m_pi), m_tau.T()*m_Xzd*m_tau);

    PL+= mult * itpp::elem_mult_sum(log(1-m_pi),m_tau.T()*m_nodiag*m_tau);




    return PL;
}

int wmixnet::f_nb_parameters_bernoulli(int Q)
{
    if(sym_done)
        return (Q*(Q+1)/2);
    else
        return (Q*Q);
}

itpp::mat wmixnet::f_residuelle_bernoulli(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::mat m_residuelle = m_X - m_tau*m_pi*m_tau.T();

    return m_residuelle;
}
