// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"


itpp::mat wmixnet::e_step(const itpp::vec & v_alpha, const itpp::mat & m_tau_r, const itpp::Vec<itpp::mat> & vm_theta)
{
    verif_model_defined();
    itpp::mat m_tau = m_tau_r;
    
    double diff;
    int iter=0;
    do
    {
        itpp::mat m_tau2;
        
        iter++;
        
//        std::cerr << iter << std::endl;
        f_E_modele(v_alpha, m_tau, vm_theta, m_tau2);

        diff = max(max(abs(m_tau2-m_tau)));

        // Renormalisation de tau par ligne
        itpp::vec v_sums = itpp::sum(m_tau2,2);
        m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

        m_tau = m_tau2;

//        std::cout << "\t\t\t" << diff << std::endl;
    } while(diff>tolF && iter<200);

//    std::cout << iter << std::endl;
    return(m_tau);
}
