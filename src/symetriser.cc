// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::symetriser()
{
    std::cerr << "Symeterisation des matrices ... ";

    if(!sym_done)
    {
        clock_t c_start = clock();

        for(int i=0;i<m_X.rows();i++)
        {
            for(int j=0;j<m_X.rows();j++)
            {
                if(m_X(i,j)!=0 || mm_Y(i,j).rows()>0)
                {
                    m_X(j,i) = m_X(i,j);
                    mm_Y(j,i) = mm_Y(i,j);
                }
            }
        }

        sym_done=true;
        
        clock_t c_end = clock();
        double tps = ((double)(c_end-c_start))/CLOCKS_PER_SEC;
        std::cerr << "done\t\t\t";
        writetmp(tps);
    }
    else
    {
        std::cerr << "skipped";
    }
    std::cerr << std::endl;

}
