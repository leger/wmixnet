// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef H_WMIXNET_PETITES_SURCHARGES
#define H_WMIXNET_PETITES_SURCHARGES 1

#include <itpp/itbase.h>
    
itpp::Vec<itpp::mat> operator+(const itpp::Vec<itpp::mat> & v_a, const itpp::Vec<itpp::mat> & v_b)
{
    if(v_a.size()!=v_b.size())
    {
        std::cerr << "SIZES !" << std::endl;
        abort();
    }

    itpp::Vec<itpp::mat> v_c(v_a.size());
    for(int i=0;i<v_a.size();i++)
        v_c(i) = v_a(i) + v_b(i);

    return(v_c);
}

itpp::Vec<itpp::mat> operator*(const itpp::Vec<itpp::mat> & v_a, const double & lambda)
{
    itpp::Vec<itpp::mat> v_r(v_a.size());

    for(int i=0;i<v_a.size();i++)
        v_r(i) = v_a(i) * lambda;

    return v_r;
}

itpp::Vec<itpp::mat> operator/(const itpp::Vec<itpp::mat> & v_a, const double & lambda)
{
    itpp::Vec<itpp::mat> v_r(v_a.size());

    for(int i=0;i<v_a.size();i++)
        v_r(i) = v_a(i) / lambda;

    return v_r;
}

itpp::Vec<itpp::mat> & operator+=(itpp::Vec<itpp::mat> & v_a, const itpp::Vec<itpp::mat> & v_b)
{
    if(v_a.size()!=v_b.size())
    {
        std::cerr << "SIZES !" << std::endl;
        abort();
    }

    for(int i=0;i<v_a.size();i++)
        v_a(i) += v_b(i);

    return(v_a);
}

itpp::Vec<itpp::mat> & operator*=(itpp::Vec<itpp::mat> & v_a, const double & lambda)
{

    for(int i=0;i<v_a.size();i++)
        v_a(i) *= lambda;

    return(v_a);
}

itpp::Vec<itpp::mat> & operator/=(itpp::Vec<itpp::mat> & v_a, const double & lambda)
{

    for(int i=0;i<v_a.size();i++)
        v_a(i) /= lambda;

    return(v_a);
}

#endif
