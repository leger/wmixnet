// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

void wmixnet::ortho_covariates()
{
    std::cerr << "Orthonormalization of covariates ... ";
    if(!ortho_done)
    {
        clock_t c_start = clock();
        int n = m_X.rows();
        int p = mm_Y(1).rows();

        itpp::mat m_Y(p,n*(n-1));
        {
            int k=0;
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<n;j++)
                {
                    if(i!=j)
                    {
                        for(int c=0;c<p;c++)
                        {
                            m_Y(c,k) = mm_Y(i,j)(c);
                        }

                        k++;
                    }
                }
            }
        }

        // On centre m_Y
        m_cov_moyennes = (1.0/(1.0*n*(n-1)))*itpp::repmat(itpp::sum(m_Y,2),1,1);
        itpp::mat m_Yc = m_Y - itpp::repmat(m_cov_moyennes,1,n*(n-1));

        // On normalise les lignes de m_Y
        itpp::mat m_cov_std = itpp::sqrt((1.0/(1.0*n*(n-1)))*itpp::repmat(itpp::sum(itpp::elem_mult(m_Yc,m_Yc),2),1,1));
        itpp::mat m_transf1 = itpp::diag(1.0/m_cov_std.get_col(0));
        m_Yc = m_transf1 * m_Yc;


        // Matrice de Gram
        itpp::mat m_Gram = (1.0/(1.0*n*(n-1))) * m_Yc * m_Yc.T();

        itpp::mat m_eigVec;
        itpp::vec v_eigVal;

        if(!itpp::eig_sym(m_Gram,v_eigVal,m_eigVec))
        {
            std::cerr << "Error on orthonormalization of covariates" << std::endl;
            abort();
        }

        double eigVal_dom = itpp::max(v_eigVal);

        double seuil = eigVal_dom/pow(2,40);

        itpp::bvec v_eig_ok = v_eigVal>seuil;

        int new_p = itpp::sum(itpp::to_vec(v_eig_ok));

        m_transfm.set_size(new_p,p);
        {
            int c1=0;
            for(int c2=0;c2<p;c2++)
            {
                if(v_eig_ok(c2))
                {
                    for(int c3=0;c3<p;c3++)
                    {
                        m_transfm(c1,c3) = m_eigVec(c3,c2) / sqrt(v_eigVal(c2));
                    }

                    c1++;
                }
            }
        }

        itpp::mat m_Yo = m_transfm * m_Yc;

        m_transfm = m_transfm * m_transf1;
        
        {
            int k=0;
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<n;j++)
                {
                    if(i!=j)
                    {
                        mm_Y(i,j).set_size(new_p,1);
                        for(int c=0;c<new_p;c++)
                        {
                            mm_Y(i,j)(c) = m_Yo(c,k);
                        }

                        k++;
                    }
                }
            }
        }

        ortho_done=true;
        
        clock_t c_end = clock();
        double tps = ((double)(c_end-c_start))/CLOCKS_PER_SEC;
        std::cerr << "done\t\t";
        writetmp(tps);

    }
    else
    {
        std::cerr << "skipped";
    }
    std::cerr << std::endl;



}
