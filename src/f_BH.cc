// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// Copyright (C) 2012, Pierre Barbillon
//               <barbillon@agroparistech.fr>, AgroParisTech, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"



/*initialisation de theta */
itpp::Vec<itpp::mat> wmixnet::f_init_BH(const itpp::mat &m_tau)
{
    int Q=m_tau.cols();
    //theta contient les pi_ql et la matrice des beta associes aux covariables
    itpp::Vec<itpp::mat> vm_theta(2); 
    itpp::mat &m_pi = vm_theta(0);
    
    itpp::mat m_prob = itpp::elem_div(m_tau.T() *m_X *m_tau, m_tau.T() * m_nodiag *m_tau);
    m_pi=log(m_prob)-log(1-m_prob);
    //std::cerr << m_pi << std::endl;
    //std::cerr << mm_Y(1) << std::endl;
    
  
    
    itpp::mat &m_beta = vm_theta(1);
    m_beta.set_size(mm_Y(1).cols(),mm_Y(1).rows()); //mm_Y(1)?
    m_beta.zeros();
      
    
    
     
    // On remplace les nan par un valeure arbitraire (0)
    for(int i=0;i<m_pi.rows();i++)
        for(int j=0;j<m_pi.cols();j++)
            if(isnan(m_pi(i,j)))
                m_pi(i,j)=0;
   
    //affichage test
    //std::cerr << m_X(0,1) << std::endl;
    //std::cerr << mm_Y(0,1) << std::endl;
    
    

    // Smooting
    borner(m_pi,-10,10);
    //std::cerr << m_pi << std::endl;
    return vm_theta;  


}



void wmixnet::f_E_BH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();
    //nb cov
    int p= mm_Y(1).rows(); 
    int Q=m_tau.cols();

    // tout d'abord nommons les choses par leurs noms.
    const itpp::mat &m_pi = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);
    
    m_tau2 = repmat(log(v_alpha),n,1,true);

 //  std::cerr << m_tau << std::endl;
//    std::cerr << mm_Y(0,1) << std::endl;
//    std::cerr << m_beta << std::endl;
    for(int i=0;i<m_tau2.rows();i++)
    {
        for(int q=0;q<m_tau2.cols();q++)
        {
            for(int j=0;j<m_tau2.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau2.cols();l++)
                    {
                        double f=0;
                       
                        double beta1=(m_beta*mm_Y(i,j))(0);
                        double beta2=(m_beta*mm_Y(j,i))(0);
                        f+=m_X(i,j)*(m_pi(q,l)+beta1)- log(1+exp(m_pi(q,l)+beta1));
                        f+=m_X(j,i)*(m_pi(l,q)+beta2)- log(1+exp(m_pi(l,q)+beta2));
                        m_tau2(i,q) += f*m_tau(j,l);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever


  

    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);
    
           
   // std::cerr << v_sums << std::endl;
    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));
    //std::cerr << m_tau2 << std::endl;

}


inline double norm_para(const itpp::vec & v_para1,const itpp::vec & v_para2)
{
    double diff = itpp::max(abs(v_para1-v_para2));
    //std::cerr << diff << std::endl;
    return diff;

}



void wmixnet::f_M_BH( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{

    
    int Q=m_tau.cols();
    int n=m_X.rows();
    vm_theta2 = vm_theta;
    itpp::mat &m_pi = vm_theta2(0);
    itpp::mat &m_beta = vm_theta2(1); 
    int p= mm_Y(1).rows();
    
   // std::cerr << p << std::endl;
    

    itpp::mat m_I(Q*Q+p,Q*Q+p); //matrice d'info de Fisher 
    itpp::vec v_U(Q*Q+p); //vecteur de score 
    itpp::vec v_cov(Q*Q+p); //vecteur des covariables     

    
   // std::cerr << m_tau << std::endl;
    itpp::vec v_para=itpp::concat(reshape(m_pi,Q*Q,1).get_col(0),m_beta.get_row(0));
   
    //la fonction reshape prend par colonne les pi
    double norm_pas_theta=INFINITY;
    double cpt=0;//sortir de la boucle ?
    do
    {    
       itpp::vec v_para_old=v_para; 
       v_U.zeros();
       m_I.zeros(); 
       v_cov.zeros();
     //  std::cerr << v_para << std::endl;
      
       for (int q=0;q<Q;q++)
       {   //std::cerr << q << std::endl;
           for (int l=0;l<Q;l++)
           {//  std::cerr << l << std::endl;
               
               v_cov.zeros();      
               v_cov(q*Q+l)=1;
             
            
               for (int i=0;i<n;i++)
               {    
                   for (int j=0;j<n;j++)
                   {
                      if(i!=j)
                      {           
                        
                         v_cov.set_subvector(Q*Q,mm_Y(i,j).get_col(0));  
                        
                        
                         double g=v_para*v_cov;
                         double prob=1/(1+exp(-g));
                         v_U+=m_tau(i,q)*m_tau(j,l)*v_cov*(m_X(i,j)-prob);
                         /*if (l==1){
                         itpp::mat temp2=  itpp::repmat(v_cov,1,1,true).T()*itpp::repmat(v_cov,1,1).T();
                         std::cerr << temp2 << std::endl;}*/
                         m_I+=m_tau(i,q)*m_tau(j,l)*( itpp::repmat(v_cov,1,1,true).T()*itpp::repmat(v_cov,1,1).T()*prob*(1-prob)); 
                      }
                   }

                }
            }
        }
      // std::cerr << v_U << std::endl;
      //std::cerr << m_I << std::endl;
       v_para+=ls_solve(m_I,v_U);  
      // std::cerr << v_para << std::endl;
      //on n'utilise pas la meme fonction que pour arreter le M a cause des types 
    //   borner(v_para,-10,10);
       cpt++;
       std::cerr << cpt << std::endl;
     //  std::cerr << v_para << std::endl;
       norm_pas_theta=norm_para(v_para,v_para_old);
       //std::cerr << norm_pas_theta << std::endl;
     }while(norm_pas_theta>tolEM );
     
     
    // std::cerr << "BH:theta0 " << vm_theta(0) << std::endl;
    
     //redonner m_pi et m_beta
     m_pi=reshape(v_para(0,Q*Q-1),Q,Q);
     m_beta=reshape(v_para(Q*Q,Q*Q+p-1),1,p);                 
     //std::cerr << "BH:theta0 " << vm_theta2(0) << std::endl;

}




double wmixnet::f_norm_theta_BH(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat &m_pi1 = vm_theta1(0);
    const itpp::mat &m_beta1 = vm_theta1(1);
    const itpp::mat &m_pi2 = vm_theta2(0);
    const itpp::mat &m_beta2 = vm_theta2(1);


    double diff = 2*itpp::max(itpp::max(abs(m_pi1-m_pi2)));
   // std::cerr << vm_theta1.size() << std::endl;
   diff+= 2*itpp::max(itpp::max(abs(m_beta1-m_beta2)));
   
    //std::cerr << diff << std::endl;
    return diff;


}


double wmixnet::f_norm2_classe_BH(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::vec v_diff = m_pi.get_col(q)-m_pi.get_col(l);
    itpp::vec v_diffr = m_pi.get_row(q)-m_pi.get_row(l);


     
    double diff = elem_mult_sum(v_diff,v_diff);
    diff+=elem_mult_sum(v_diffr,v_diffr);
   
    return diff;
  


}


double wmixnet::f_PL_BH(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);
    const itpp::mat &m_beta = vm_theta(1);

    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));
       
    int Q=m_tau.cols();
  
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                double beta1=(m_beta*mm_Y(i,j))(0);
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        double f=0;
                        
                        
                        
                        f+=m_X(i,j)*(m_pi(q,l)+beta1)- log(1+exp(m_pi(q,l)+beta1));
                        
                        PL += f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever
  //  std::cerr << J << std::endl;
    return PL;    


}



int wmixnet::f_nb_parameters_BH(int Q)
{
    int p= mm_Y(1).rows();
    if(sym_done)
        return (Q*(Q+1)/2+p);
    else
        return (Q*Q+p);

      
}


itpp::mat wmixnet::f_residuelle_BH(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_pi = vm_theta(0);

    itpp::mat m_residuelle = m_X - m_tau*m_pi*m_tau.T();

    return m_residuelle;
}
