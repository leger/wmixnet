// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

itpp::bvec wmixnet::smoothing_pbs()
{
    verif_em_with_init();

    // Detection des problemes
    itpp::bvec v_pbs(Qmax-Qmin+1);

    for(int Q=Qmin;Q<=Qmax;Q++)
    {
        if(Q>Qmin)
            v_pbs(Q-Qmin) = (v_J(Q-Qmin)<v_J(Q-1-Qmin));
        else
            v_pbs(Q-Qmin)=false;
        if(Q>Qmin && Q<Qmax)
        {
            if(!v_pbs(Q-Qmin))
                if(v_ICL(Q-Qmin-1)+v_ICL(Q-Qmin+1)>2*v_ICL(Q-Qmin))
                    v_pbs(Q-Qmin)=true;
        }
    }
    return v_pbs;
}

struct wmixnet::job_smoothing_ascend
{
    wmixnet* p_obj;
    itpp::mat * pm_Z;
    itpp::mat * pm_coords;
    int Q;
    int q;
    void operator()()
    {
        if(itpp::sum(pm_Z->get_col(q))>1)
        {
            itpp::ivec v_indexes = itpp::find(pm_Z->get_col(q)==1);
            itpp::Vec<itpp::ivec> vv_cluster = p_obj->spectral_clustering_kmeans(*pm_coords,v_indexes,Q,2);

            itpp::mat m_tau_tmp;
            m_tau_tmp.set_size(pm_Z->rows(),Q);
            m_tau_tmp.zeros();

            int c=0;
            for(int i=0;i<Q-1;i++)
                if(i!=q)
                    m_tau_tmp.set_col(c++,pm_Z->get_col(i));

            // Il reste a mettre les colonnes Q-2 et Q-1
            for(int t=0;t<vv_cluster(0).length();t++)
                m_tau_tmp(vv_cluster(0)(t),Q-2)=1;
            for(int t=0;t<vv_cluster(1).length();t++)
                m_tau_tmp(vv_cluster(1)(t),Q-1)=1;

            borner(m_tau_tmp,p_obj->tolP,1-p_obj->tolP);

            /*
            itpp::mat m_Ztmp = p_obj->hierarchical_clustering_internal(itpp::find(pm_Z->get_col(q)==1));
            itpp::mat m_tau_tmp;
            m_tau_tmp.set_size(pm_Z->rows(),Q);

            int c=0;
            for(int i=0;i<Q-1;i++)
                if(i!=q)
                    m_tau_tmp.set_col(c++,pm_tau_prec->get_col(i));

            m_tau_tmp.set_col(c++,itpp::elem_mult(m_Ztmp.get_col(0),pm_tau_prec->get_col(q)));
            m_tau_tmp.set_col(c++,itpp::elem_mult(m_Ztmp.get_col(1),pm_tau_prec->get_col(q)));*/
            

            // Go
            p_obj->em(m_tau_tmp, Q);
        }
    }

};

struct wmixnet::job_smoothing_descend
{
    wmixnet* p_obj;
    int Q;
    int qmin;
    int lmin;

    void operator()()
    {
        const itpp::mat & m_tau_suiv = p_obj->vm_tau(Q-p_obj->Qmin+1);
        itpp::mat m_tau_tmp;
        m_tau_tmp.set_size(m_tau_suiv.rows(),Q);

        int c=0;
        for(int q=0;q<Q+1;q++)
            if(q!=qmin && q!=lmin)
                m_tau_tmp.set_col(c++,m_tau_suiv.get_col(q));

        m_tau_tmp.set_col(c++,m_tau_suiv.get_col(qmin)+m_tau_suiv.get_col(lmin));

        p_obj->em(m_tau_tmp,Q);
    }
};

bool wmixnet::smoothing_ascend(int Q)
{
    std::cerr << "Smoothing (ascend)  for Q = " << Q << " ... ";
    double old_J = v_J(Q-Qmin);

    boost::timer::cpu_timer timer;
    timer.start();

    // Allons y

    itpp::mat & m_tau_prec = vm_tau(Q-Qmin-1);
    itpp::Vec<itpp::mat> & vm_theta_prec = vvm_theta(Q-Qmin-1);
    itpp::mat m_Z = tau_to_Z(m_tau_prec);

    itpp::mat m_coords = spectral_clustering_coords(f_residuelle_modele(m_tau_prec,vm_theta_prec));

    // On tente de diviser chacun des clusters precedens

    int & nthreads = parallel;

    if(parallel>1)
    {
        // On cree un tableau de threads
        boost::thread* tab = new boost::thread[nthreads];
        for(int q=0;q<Q-1;q++)
        {
            // On boucle jusqu'a trouver un thread de libre
            int num=0;
            do
            {
                num++;
                num=num%nthreads;
            } while(!( tab[num].timed_join(boost::posix_time::milliseconds(10)) ));
            // num est un thread libre ! Au boulot feignasse !
            struct wmixnet::job_smoothing_ascend le_job;
            le_job.p_obj = this;
            le_job.pm_Z = &m_Z;
            le_job.pm_coords = &m_coords;
            le_job.Q = Q;
            le_job.q = q;
            tab[num] = boost::thread(le_job);
        }
        // Maintenant on attend que tous les jobs aient fini
        for(int num=0;num<nthreads;num++)
        {
            tab[num].join();
        }
        // Done
        delete[] tab;
    }
    else
    {
        for(int q=0;q<Q-1;q++)
        {
            struct wmixnet::job_smoothing_ascend le_job;
            le_job.p_obj = this;
            le_job.pm_Z = &m_Z;
            le_job.pm_coords = &m_coords;
            le_job.Q = Q;
            le_job.q = q;
            le_job();
        }
    }

    bool r = (v_J(Q-Qmin)>old_J);
    std::cerr << "done ";
    if(r)
        std::cerr << "(better) ";
    else
        std::cerr << "         ";
    std::cerr << "\t";
    timer.stop();
    double tps=(double)(timer.elapsed().user+ timer.elapsed().system)/1000000000LL;
    wmixnet::writetmp(tps);
    if(r)
        std::cerr << " J=" << v_J(Q-Qmin);
    std::cerr << std::endl;

    return r;

}


bool wmixnet::smoothing_descend(int Q)
{
    
    std::cerr << "Smoothing (descend) for Q = " << Q << " ... ";
    double old_J = v_J(Q-Qmin);

    boost::timer::cpu_timer timer;
    timer.start();

    // Calculons la matrices des distances interclasses
    itpp::vec v_dists((Q+1)*(Q)/2);
    itpp::ivec v_qs((Q+1)*(Q)/2);
    itpp::ivec v_ls((Q+1)*(Q)/2);
    int k=0;
    for(int q=0;q<Q;q++)
    {
        for(int l=q+1;l<Q+1;l++)
        {
            v_dists(k)=f_norm2_classe_modele(vvm_theta(Q+1-Qmin),q,l);
            v_qs(k)=q;
            v_ls(k)=l;
            k++;
        }
    }

    itpp::ivec v_order = itpp::sort_index(v_dists);

    
    int & nthreads = parallel;

    if(parallel>1)
    {
        // On cree un tableau de threads
        boost::thread* tab = new boost::thread[nthreads];
        for(int k=0;k<Q;k++)
        {
            // On boucle jusqu'a trouver un thread de libre
            int num=0;
            do
            {
                num++;
                num=num%nthreads;
            } while(!( tab[num].timed_join(boost::posix_time::milliseconds(10)) ));
            // num est un thread libre ! Au boulot feignasse !
            int qmin=v_qs(k);
            int lmin=v_ls(k);
            struct wmixnet::job_smoothing_descend le_job;
            le_job.p_obj = this;
            le_job.Q = Q;
            le_job.qmin = qmin;
            le_job.lmin = lmin;
            tab[num] = boost::thread(le_job);
        }
        // Maintenant on attend que tous les jobs aient fini
        for(int num=0;num<nthreads;num++)
        {
            tab[num].join();
        }
        // Done
        delete[] tab;
    }
    else
    {
        for(int k=0;k<Q;k++)
        {
            int qmin=v_qs(k);
            int lmin=v_ls(k);
            struct wmixnet::job_smoothing_descend le_job;
            le_job.p_obj = this;
            le_job.Q = Q;
            le_job.qmin = qmin;
            le_job.lmin = lmin;
            le_job();
        }
    }


    timer.stop();
    double tps=(double)(timer.elapsed().user+ timer.elapsed().system)/1000000000LL;
    bool r = (v_J(Q-Qmin)>old_J);
    std::cerr << "done ";
    if(r)
        std::cerr << "(better) ";
    else
        std::cerr << "         ";
    std::cerr << "\t";
    writetmp(tps);
    if(r)
        std::cerr << " J=" << v_J(Q-Qmin);
    std::cerr << std::endl;

    return r;
}


void wmixnet::smoothing()
{
    if(smoothing_mode!="none")
    {


        while(true)
        {
            itpp::bvec v_pbs=smoothing_pbs();
            bool descend=true;
            int Qtrouve=-1;

            for(int Q=Qmin;Q<Qmax;Q++)
                if(v_last_descend(Q-Qmin)!=v_id(Q+1-Qmin))
                    if(smoothing_mode=="exhaustive" || v_pbs(Q-Qmin))
                        Qtrouve=Q;

            if(Qtrouve<0)
            {
                descend=false;
                for(int Q=Qmax;Q>Qmin;Q--)
                    if(v_last_ascend(Q-Qmin)!=v_id(Q-1-Qmin))
                        if(smoothing_mode=="exhaustive" || v_pbs(Q-Qmin))
                            Qtrouve=Q;
            }

            if(Qtrouve<0)
                break;


            bool success=false;

            if(descend)
            {
                success=smoothing_descend(Qtrouve);
                v_last_descend(Qtrouve-Qmin) = v_id(Qtrouve+1-Qmin);
            }
            else
            {
                success=smoothing_ascend(Qtrouve);
                v_last_ascend(Qtrouve-Qmin) = v_id(Qtrouve-1-Qmin);
            }

            if(success)
               save_state();

        }
    }
}

        



            





