// Copyright (C) 2012, Jean-Benoist Leger
//                           <jleger@agroparistech.fr>, INRA, France
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "wmixnet.h"

inline double lfphi(double x,double m, double s2)
{
        return(-0.5*log(2*M_PI*s2)-0.5*(x-m)*(x-m)/s2);
}


itpp::Vec<itpp::mat> wmixnet::f_init_gaussian(const itpp::mat &m_tau)
{
    itpp::Vec<itpp::mat> vm_theta(2);
    itpp::mat &m_mu = vm_theta(0);
    vm_theta(1).set_size(1,1);
    double &sigma2 = vm_theta(1)(0);


    itpp::mat m_A = m_tau.T() *m_Xzd *m_tau;
    itpp::mat m_B = m_tau.T() *m_nodiag *m_tau;

    m_mu = itpp::elem_div(m_A, m_B);
    
    
    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_mu.rows();i++)
        for(int j=0;j<m_mu.cols();j++)
            if(isnan(m_mu(i,j)))
                m_mu(i,j)=0;

    int n = m_X.rows();
    double s=0;
    s+= itpp::sumsum(m_tau.T()*m_Xzd2*m_tau);
    s+= itpp::elem_mult_sum(itpp::elem_mult(m_mu,m_mu),m_B);
    s+= -2*itpp::elem_mult_sum(m_mu, m_A);
    s/= n*(n-1);

    sigma2=s;

    return vm_theta;

}

void wmixnet::f_E_gaussian( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> vm_theta, itpp::mat & m_tau2)
{
    int n=m_X.rows();

    // tout d'abort nomons les choses par leurs noms.
    const itpp::mat &m_mu = vm_theta(0);
    const double &sigma2 = vm_theta(1)(0);
    
    m_tau2 = repmat(log(v_alpha),n,1,true);
    m_tau2+= -(-2*m_Xzd*m_tau*m_mu.T() + m_nodiag*m_tau*itpp::elem_mult(m_mu,m_mu).T() )/2/sigma2;
    if(!sym_done)
        m_tau2+= -(-2*m_XzdT*m_tau*m_mu + m_nodiag*m_tau*itpp::elem_mult(m_mu,m_mu) )/2/sigma2;

    // On enleve la composante moyenne
    m_tau2-=itpp::repmat(itpp::max(m_tau2,2), 1, m_tau2.cols());

    borner(m_tau2,-200,0);

    m_tau2=exp(m_tau2);

    // Renormalisons tau par ligne
    itpp::vec v_sums = itpp::sum(m_tau2,2);

    m_tau2 = itpp::elem_div(m_tau2, itpp::repmat(v_sums, 1, m_tau.cols()));

}

void wmixnet::f_M_gaussian( const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta, itpp::Vec<itpp::mat> & vm_theta2)
{
    vm_theta2.set_size(2);
    itpp::mat &m_mu = vm_theta2(0);
    vm_theta2(1).set_size(1,1);
    double &sigma2 = vm_theta2(1)(0);


    itpp::mat m_A = m_tau.T() *m_Xzd *m_tau;
    itpp::mat m_B = m_tau.T() *m_nodiag *m_tau;

    m_mu = itpp::elem_div(m_A, m_B);
    
    
    // On remplace les nan par une valeure arbitraire (0)
    for(int i=0;i<m_mu.rows();i++)
        for(int j=0;j<m_mu.cols();j++)
            if(isnan(m_mu(i,j)))
                m_mu(i,j)=0;

    int n=m_X.rows();
    double s=0;
    s+= itpp::sumsum(m_tau.T()*m_Xzd2*m_tau);
    s+= itpp::elem_mult_sum(itpp::elem_mult(m_mu,m_mu),m_B);
    s+= -2*itpp::elem_mult_sum(m_mu, m_A);
    s/= n*(n-1);

    sigma2=s;


}

double wmixnet::f_norm_theta_gaussian(const itpp::Vec<itpp::mat> & vm_theta1,const itpp::Vec<itpp::mat> & vm_theta2)
{
    const itpp::mat & m_mu1 = vm_theta1(0);
    const itpp::mat & m_mu2 = vm_theta2(0);
    const double &sigma21 = vm_theta1(1)(0);
    const double &sigma22 = vm_theta2(1)(0);

    double diff = itpp::max(itpp::max(abs(m_mu1-m_mu2))) + sqrt(abs(sigma21-sigma22));

    return diff;
}

double wmixnet::f_norm2_classe_gaussian(const itpp::Vec<itpp::mat> & vm_theta, int q, int l)
{
    const itpp::mat & m_mu = vm_theta(0);
    itpp::vec v_diff = m_mu.get_col(q)-m_mu.get_col(l);
    itpp::vec v_diffr = m_mu.get_row(q)-m_mu.get_row(l);

    double diff = elem_mult_sum(v_diff,v_diff);
    diff += elem_mult_sum(v_diffr,v_diffr);

    return diff;
}

double wmixnet::f_PL_gaussian(const itpp::vec & v_alpha, const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
    const itpp::mat & m_mu = vm_theta(0);
    const double & sigma2 = vm_theta(1)(0);

    double n = m_X.rows();
    double PL=0;

    // PL
    PL+= itpp::sumsum(m_tau*itpp::mat(log(v_alpha)));

    double mult = (sym_done) ? .5 : 1;
    for(int i=0;i<m_tau.rows();i++)
    {
        for(int q=0;q<m_tau.cols();q++)
        {
            for(int j=0;j<m_tau.rows();j++)
            {
                if(i!=j)
                {
                    for(int l=0;l<m_tau.cols();l++)
                    {
                        double f=lfphi(m_X(i,j),m_mu(q,l),sigma2);
                        PL += mult*f*m_tau(j,l)*m_tau(i,q);
                    }
                }
            }
        }
    } //OUTCH LA BOUCLE, TODO : l'enlever
    //J+= -n*(n-1)/4*(log(2*M_PI) + log(sigma2) +1);
    return PL;
}

int wmixnet::f_nb_parameters_gaussian(int Q)
{
    if(sym_done)
       return (1+Q*(Q+1)/2);
    else
        return (1+Q*Q);
}

itpp::mat wmixnet::f_residuelle_gaussian(const itpp::mat & m_tau, const itpp::Vec<itpp::mat> & vm_theta)
{
        const itpp::mat & m_mu = vm_theta(0);
        itpp::mat m_residuelle = m_X - m_tau*m_mu*m_tau.T();
        return m_residuelle;
}

